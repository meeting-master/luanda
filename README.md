#luanda : Meeting Master Mobile APP

#Installation
 - npm install
 - npx pod-install (for iOS)
 
 #Run
 - npm run android
 - npm run ios
 
 #Run against a specific device
 - react-native run-ios --simulator='iPhone 11' // for use iPhone 11
 - react-native run-android --deviceId=YOUR_DEVICE_ID // adb devices -l
 
#Nic nac
- Get android emulator list :  emulator -list-avds
- Start emulator : emulator -avd avd_name (emulator -avd Pixel_3_API_28)

#Babel Runtime issue
- npm add @babel/runtime
- npm install

#Troubleshoot
 1. Clear watchman watches: watchman watch-del-all
 2. Delete node_modules: rm -rf node_modules && npm install
 3. npm cache clean --force
 4. ./android/gradlew clean -p
 5. Reset Metro's cache: npm start --reset-cache
 6. Remove the cache: rm -rf /tmp/metro-*


#Android Release (make sur you have keystore file and keys)

1. cd android
2. ./gradlew bundleRelease

#clean pods

1. rm -rf ~/Library/Caches/CocoaPods
2. rm -rf Pods
3. rm -rf ~/Library/Developer/Xcode/DerivedData/*
4. npx pod-install