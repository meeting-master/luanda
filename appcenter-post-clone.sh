#!/usr/bin/env bash

if [ "$APPCENTER_BRANCH" == "production" ] || [ "$1" == "production" ];
then
  echo "Switching to Production environment"
  yes | cp -rf "environments/production/google-services-prod.json" "android/app/google-services.json"
  yes | cp -rf "environments/production/android-prod.keystore" "android/app/"
  yes | cp -rf "environments/production/GoogleService-Info.plist" ios
else
  echo "Switching to Dev environment"
  yes | cp -rf "environments/test/google-services-test.json" "android/app/google-services.json"
  yes | cp -rf "environments/test/android-test.keystore" "android/app/"
  yes | cp -rf "environments/test/GoogleService-Info.plist" ios
fi