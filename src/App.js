import 'react-native-gesture-handler';
import React, {useEffect, useState} from 'react';
import {Alert, StatusBar, StyleSheet, View} from 'react-native';
import messaging from '@react-native-firebase/messaging';
import NavigationContainer from '@react-navigation/native/src/NavigationContainer';
import StackNavigator from './navigation/StackNavigator';
import {Provider} from 'react-redux';
import {isIos} from './constants/PlatformService';
import SplashScreen from 'react-native-splash-screen';

import {initStore} from './models/store';
import './translation/i18n';
import CustomAlert from './components/CustomAlert';

const store = initStore();
const App = () => {
  const [showAlert, setShowAlert] = useState(false);
  const [alertMessage, setAlertMessage] = useState();
  useEffect(() => {
    SplashScreen.hide();
  });
  useEffect(() => {
    return messaging().onMessage(async (remoteMessage) => {
      setAlertMessage(remoteMessage.notification.body);
      setShowAlert(true);
    });
  }, []);

  return (
    <>
      <View style={styles.container}>
        <StatusBar
          barStyle={isIos() ? 'dark-content' : 'default'}
          translucent={false}
        />
        <Provider store={store}>
          <NavigationContainer>
            <StackNavigator />
          </NavigationContainer>
        </Provider>
      </View>
      {alertMessage && showAlert && (
        <CustomAlert
          message={alertMessage}
          showAlert={showAlert}
          confirmText="OK"
          onCancelPressed={() => {
            setShowAlert(false);
          }}
          onConfirmPressed={() => {
            setShowAlert(false);
            setAlertMessage();
          }}
        />
      )}
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});

export default App;
