import React, {useState} from 'react';
import {StyleSheet} from 'react-native';
import {Button} from 'react-native-elements';

//hooks
import {useTranslation} from '../hooks/useTranslation';

//components
import Colors from '../constants/Colors';
import FONTS from '../constants/fonts';
import {IonIcon, MaterialIcon} from '../components/Icon';
import PropTypes from 'prop-types';
import CustomModal from './CustomModal';

/*
props:
  onDeleteConfirmShow
  onDeleteConfrimHide
  confirmDuration
  onClick
  disabled

*/
const ButtonWithConfirmation = ({
  onClick,
  onDeleteConfirmShow,
  onDeleteConfirmHide,
  confirmDuration,
  disabled,
}) => {
  const {t} = useTranslation();
  const [confirming, setConfirming] = useState(false);

  const setConfirmationStatus = () => {
    setConfirming(true);
    if (onDeleteConfirmShow) {
      onDeleteConfirmShow();
    }

    setTimeout(() => {
      setConfirming(false);
      if (onDeleteConfirmHide) {
        onDeleteConfirmHide();
      }
    }, confirmDuration || 4000);
  };

  const handleConfirmation = () => {
    onClick();
    clearTimeout();
    setConfirming(false);
    if (onDeleteConfirmHide) {
      onDeleteConfirmHide();
    }
  };

  if (!confirming) {
    return (
      <Button
        onPress={() => setConfirmationStatus()}
        type={'clear'}
        titleStyle={styles.deleteButtonTitle}
        icon={
          <MaterialIcon
            name="delete-forever"
            size={22}
            color={disabled ? Colors.grey : Colors.royalblue}
          />
        }
        disabled={disabled}
        style={{top: -10}}
      />
    );
  }
  return (
    <Button
      title={t('common.buttonWithConfirmation')}
      onPress={() => handleConfirmation()}
      type={'outline'}
      titleStyle={styles.confirmButtonTitle}
      icon={
        <MaterialIcon
          name="delete-forever"
          size={20}
          color={Colors.errorBackground}
        />
      }
      buttonStyle={styles.confirmButton}
    />
  );
};

ButtonWithConfirmation.propTypes = {
  onDeleteConfirmShow: PropTypes.func,
  onDeleteConfirmHide: PropTypes.func,
  onClick: PropTypes.func,
  confirmDuration: PropTypes.number,
  disabled: PropTypes.bool,
  testId: PropTypes.string,
};

const styles = StyleSheet.create({
  deleteButtonTitle: {
    fontFamily: FONTS.OpenSans,
    fontSize: 12,
    color: Colors.primary,
  },
  confirmButtonTitle: {
    fontFamily: FONTS.OpenSans,
    fontSize: 12,
    color: Colors.errorBackground,
  },
  confirmButton: {
    borderColor: Colors.errorBackground,
  },
});

export default ButtonWithConfirmation;
