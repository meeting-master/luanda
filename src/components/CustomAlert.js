import React, {useState} from 'react';
import {StyleSheet} from 'react-native';

//components
import Colors from '../constants/Colors';
import FONTS from '../constants/fonts';
import PropTypes from 'prop-types';

import AwesomeAlert from 'react-native-awesome-alerts';
import {useFocusEffect} from '@react-navigation/core';

const CustomAlert = ({
  message,
  showAlert,
  showCancelButton,
  cancelText,
  cancelButtonColor,
  confirmText,
  confirmButtonColor,
  onConfirmPressed,
  onCancelPressed,
}) => {
  return (
    <AwesomeAlert
      show={showAlert}
      showProgress={false}
      title="Easy Meeting"
      message={message}
      closeOnTouchOutside={false}
      closeOnHardwareBackPress={false}
      showCancelButton={showCancelButton}
      showConfirmButton={true}
      cancelText={cancelText}
      confirmText={confirmText}
      confirmButtonColor={confirmButtonColor}
      cancelButtonColor={cancelButtonColor}
      onCancelPressed={onCancelPressed}
      onConfirmPressed={onConfirmPressed}
      contentContainerStyle={styles.contentContainerStyle}
      titleStyle={styles.titleStyle}
      messageStyle={styles.messageStyle}
    />
  );
};

CustomAlert.propTypes = {
  message: PropTypes.string.isRequired,
  showAlert: PropTypes.bool,
  showCancelButton: PropTypes.bool.isRequired,
  cancelText: PropTypes.string,
  cancelButtonColor: PropTypes.string,
  confirmText: PropTypes.string.isRequired,
  confirmButtonColor: PropTypes.string.isRequired,
  onCancelPressed: PropTypes.func.isRequired,
  onConfirmPressed: PropTypes.func.isRequired,
};

CustomAlert.defaultProps = {
  showCancelButton: false,
  confirmButtonColor: Colors.meanBlue,
  showAlert: false,
};

const styles = StyleSheet.create({
  titleStyle: {
    fontFamily: FONTS.OpenSans,
    fontWeight: 'bold',
    fontSize: 12,
    color: Colors.meanBlue,
  },
  messageStyle: {
    fontFamily: FONTS.OpenSans,
    fontSize: 12,
    color: Colors.darkGrayTwo,
  },
  contentContainerStyle: {
    backgroundColor: Colors.clearGray,
    shadowColor: Colors.black,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
});

export default CustomAlert;
