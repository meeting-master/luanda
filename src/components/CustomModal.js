import React, {useEffect, useState} from 'react';
import {Animated, TouchableWithoutFeedback, StyleSheet} from 'react-native';
import PropTypes from 'prop-types';
import Modal from 'react-native-modal';

// services
import {isIos, Version} from '../constants/PlatformService';

// constants
import OPACITIES from '../constants/opacities';
import Colors from '../constants/Colors';

const CustomModal = ({
  children,
  isVisible,
  onCloseModal,
  optimized,
  testId,
}) => {
  const isIosBreakingChanges = isIos() && Version() >= 13;
  const additionalProps = isIosBreakingChanges
    ? {presentationStyle: 'pageSheet'}
    : {};
  const defaultOpacity = isVisible ? 1 : 0;

  const [opacity] = useState(new Animated.Value(defaultOpacity));
  const [modalVisible, setModalVisible] = useState(isVisible);

  // Works only for iOS 13+ modals
  /* istanbul ignore next */
  const dismissModal = (e) => {
    if (isIosBreakingChanges && e.nativeEvent.locationY < 0) {
      onCloseModal();
    }
  };

  useEffect(() => {
    const toValue = isVisible ? 1 : 0;
    const animation = {duration: 300, useNativeDriver: true, toValue};

    Animated.timing(opacity, animation).start(() => {
      setModalVisible(isVisible);
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isVisible]);

  const renderModal = () => {
    return (
      <Modal
        testID={testId}
        style={styles.modal}
        hideModalContentWhileAnimating
        backdropColor={Colors.black}
        backdropOpacity={OPACITIES.modalBackDrop}
        onBackdropPress={onCloseModal}
        onBackButtonPress={onCloseModal}
        isVisible={isVisible}
        {...additionalProps}>
        <TouchableWithoutFeedback onPressOut={dismissModal}>
          {children}
        </TouchableWithoutFeedback>
      </Modal>
    );
  };

  return (
    <>
      {optimized && (
        <Animated.View style={{opacity}}>
          {modalVisible && renderModal()}
        </Animated.View>
      )}
      {!optimized && renderModal()}
    </>
  );
};

CustomModal.defaultProps = {
  isVisible: false,
};

CustomModal.propTypes = {
  children: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  isVisible: PropTypes.bool,
  onCloseModal: PropTypes.func.isRequired,
  optimized: PropTypes.bool,
  testId: PropTypes.string,
};

CustomModal.defaultProps = {
  optimized: true,
};

const styles = StyleSheet.create({
  modal: {
    margin: 0,
  },
});

export default CustomModal;
