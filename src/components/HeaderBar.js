import React from 'react';
import {Text, View, StyleSheet, TouchableOpacity} from 'react-native';
import Colors from '../constants/Colors';
import FONTS from '../constants/fonts';
import {Divider} from 'react-native-elements';
import {MaterialIcon} from './Icon';
import PropTypes from 'prop-types';

const HeaderBar = ({backPress, screenName, rightComponent}) => {
  return (
    <>
      <View style={styles.container}>
        <View style={styles.bar}>
          <TouchableOpacity onPress={backPress} style={styles.back}>
            <MaterialIcon 
              name="arrow-back" 
              size={20} 
              color={Colors.white}
            />
          </TouchableOpacity>
          <Text style={styles.text}>{screenName}</Text>
          <View style={styles.right}>{rightComponent}</View>
        </View>
      </View>
      <Divider />
    </>
  );
};

HeaderBar.propTypes = {
  backPress: PropTypes.func.isRequired,
  rightComponent: PropTypes.object,
  screenName: PropTypes.string,
};
const styles = StyleSheet.create({
  container: {
    height: 55,
    backgroundColor: Colors.meanBlue,
  },
  bar: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  back: {
    position: 'absolute',
    left: 0,
    margin: 10,
  },
  text: {
    color: Colors.white,
    fontFamily: FONTS.OpenSansBold,
    fontSize: 14,
    marginLeft: 40,
  },
  right: {
    position: 'absolute',
    right: 0,
    margin: 10,
  },
});

export default HeaderBar;
