import * as React from 'react';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';

import FontAwesome from 'react-native-vector-icons/FontAwesome';

export function MaterialIcon({color, name, size, style}) {
  return <MaterialIcons name={name} size={size} style={style} color={color} />;
}

export function IonIcon({color, name, size, style}) {
  return <Ionicons name={name} size={size} style={style} color={color} />;
}

export function Awesome({color, name, size, style}) {
  return <FontAwesome name={name} size={size} style={style} color={color} />;
}
