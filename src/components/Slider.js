import React, {useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {Slider} from 'react-native-elements';
import PropTypes from 'prop-types';

//constants
import Colors from '../constants/Colors';
import FONTS from '../constants/fonts';

const EasySlider = ({onChange, label}) => {
  const radius = 0.5;
  const radiusMultiplier = 100;
  const [radiusValue, setRadiusValue] = useState(50);

  const handleRadiusChange = (value) => {
    const v = Math.floor(value * radiusMultiplier);
    setRadiusValue(v);
    onChange(v);
  };

  return (
    <View style={styles.sliderContainer}>
      <Text style={styles.sliderText}>{label}</Text>
      <Slider
        thumbTintColor={Colors.black}
        style={styles.slider}
        value={radius}
        onValueChange={handleRadiusChange}
      />
      <Text style={styles.sliderText}>{radiusValue}</Text>
    </View>
  );
};

EasySlider.propTypes = {
  onChange: PropTypes.func.isRequired,
  label: PropTypes.string,
};

const styles = StyleSheet.create({
  sliderContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 20,
  },
  sliderText: {
    fontFamily: FONTS.OpenSansItalic,
    fontSize: 12,
    marginHorizontal: 10,
  },
  slider: {
    width: '70%',
  },
});

export default EasySlider;
