import React from 'react';
import {
  FlatList,
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  View,
} from 'react-native';
import {ListItem} from 'react-native-elements';

//hook
import {useTranslation} from '../hooks/useTranslation';

//services
import Colors from '../constants/Colors';
import {IonIcon} from './Icon';
import FONTS from '../constants/fonts';
import format from 'date-fns/format';

//components
import Rating from './rating';
import EmptyList from './emptyList';

const Comment = ({onClose, comments}) => {
  const {t} = useTranslation();
  const renderRatingItem = ({item}) => (
    <ListItem
      title={renderDate(item?.createdAt)}
      titleStyle={styles.listTitle}
      containerStyle={styles.listItem}
      subtitle={item.comment}
      rightTitle={<Rating value={item.value} />}
    />
  );

  const renderDate = (date) => {
    return format(Date.parse(date), t('date_format'));
  };

  const renderRatings = () => (
    <FlatList
      data={comments}
      renderItem={renderRatingItem}
      keyExtractor={(item) => item.id}
      ListEmptyComponent={<EmptyList message={t('empty_comment')} />}
    />
  );

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.header}>
        <TouchableOpacity onPress={onClose}>
          <IonIcon name="close" color={Colors.grey} size={30} />
        </TouchableOpacity>
      </View>
      <View style={styles.body}>{renderRatings()}</View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: Colors.greyhish,
  },
  header: {
    height: 50,
    alignSelf: 'flex-start',
    backgroundColor: Colors.white,
    width: '100%',
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderColor: Colors.light,
    padding: 10,
  },
  body: {
    margin: 10,
    alignSelf: 'baseline',
    width: '95%',
    flex: 1,
  },
  listItem: {
    borderRadius: 10,
    marginVertical: 5,
  },
  listTitle: {
    fontFamily: FONTS.OpenSans,
    color: Colors.primary,
    fontSize: 13,
    textAlign: 'justify',
  },
});

export default Comment;
