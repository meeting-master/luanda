import * as React from 'react';
import {Text, View, StyleSheet} from 'react-native';
import FONTS from '../constants/fonts';

const EmptyList = ({message}) => {
  return (
    <View style={styles.container}>
      <Text style={styles.message}>{message}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginHorizontal: 10,
    marginVertical: 10,
    alignItems: 'center',
  },
  message: {
    fontFamily: FONTS.OpenSans,
    fontSize: 14,
  },
});
export default EmptyList;
