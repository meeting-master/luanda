import React from 'react';
import Colors from '../constants/Colors';
import {AirbnbRating} from 'react-native-ratings';

const Rating = ({value}) => {
  const roundedValue = Math.round(value);
  const ratingColor = () => {
    switch (roundedValue) {
      case 1:
        return Colors.rating.execrable;
      case 2:
        return Colors.rating.bad;
      case 3:
        return Colors.rating.medium;
      case 4:
        return Colors.rating.prettyGood;
      case 5:
        return Colors.rating.good;
      default:
        return Colors.dark;
    }
  };

  return (
    <AirbnbRating
      showRating={false}
      selectedColor={Colors.royalblue}
      isDisabled={true}
      defaultRating={roundedValue}
      size={12}
      count={5}
      reviewSize={10}
      starContainerStyle={{alignSelf: 'flex-start'}}
    />
  );
};

export default Rating;
