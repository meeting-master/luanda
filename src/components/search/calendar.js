import React from 'react';
import {StyleSheet, View} from 'react-native';
import {Calendar} from 'react-native-calendars';

//service
import Colors from '../../constants/Colors';
import FONTS from '../../constants/fonts';

import '../../translation/localCalendar';

const MCalendar = ({
  onDayPress,
  selectedDay,
  onMonthChange,
  dateWithAvailableTimeSlots,
}) => {
  let markedAvailableDate = {};
  dateWithAvailableTimeSlots.forEach((day) => {
    markedAvailableDate[day] = {
      marked: true,
    };
  });
  markedAvailableDate[selectedDay] = {selected: true};

  return (
    <View style={styles.calenderContainer}>
      <Calendar
        style={styles.calender}
        onDayPress={onDayPress}
        onMonthChange={onMonthChange}
        theme={{
          monthTextColor: Colors.royalblue,
          arrowColor: Colors.royalblue,
          todayTextColor: Colors.royalblue,
          selectedDayTextColor: Colors.whiteTwo,
          selectedDayBackgroundColor: Colors.meanBlue,
          'stylesheet.day.basic': {
            base: {
              height: 25,
              width: 25,
              alignItems: 'center',
            },
          },
        }}
        markedDates={markedAvailableDate}
        minDate={new Date()}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: Colors.greyhish,
  },
  calenderContainer: {
    marginVertical: 10,
    marginHorizontal: 10,
    width: '90%',
    fontFamily: FONTS.OpenSansBold,
  },
  calender: {
    borderRadius: 10,
  },
});
export default MCalendar;
