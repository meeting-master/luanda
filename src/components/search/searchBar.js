import React from 'react';
import {Platform, StyleSheet} from 'react-native';
import {SearchBar} from 'react-native-elements';
import PropTypes from 'prop-types';

//constant
import FONTS from '../../constants/fonts';

const EasySearchBar = ({
  value,
  onChangeText,
  onKeyPress,
  cancelTitle,
  placeHolder,
  searchIcon,
  onFocus,
  onCancel,
  hasElevation,
}) => {
  return (
    <SearchBar
      lightTheme
      showLoading={false}
      platform={Platform.OS}
      cancelButtonTitle={cancelTitle}
      placeholder={placeHolder}
      onChangeText={onChangeText}
      value={value}
      onSubmitEditing={onKeyPress}
      containerStyle={hasElevation ? styles.containerWithElevation : styles.container}
      inputStyle={styles.searchText}
      inputContainerStyle={styles.input}
      searchIcon={searchIcon}
      onFocus={onFocus}
      onCancel={onCancel}
    />
  );
};

EasySearchBar.propTypes = {
  onChangeText: PropTypes.func.isRequired,
  onCancel: PropTypes.func,
  onKeyPress: PropTypes.func,
  onFocus: PropTypes.func,
  value: PropTypes.string,
  cancelTitle: PropTypes.string,
  placeHolder: PropTypes.string,
  hasElevation: PropTypes.bool,
};

EasySearchBar.defaultProps = {
  hasElevation: false,
};

const styles = StyleSheet.create({
  container: {
    borderRadius: 5,
    height: 45,
    alignContent: 'center',
  },
  containerWithElevation: {
    borderRadius: 5,
    height: 45,
    alignContent: 'center',
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  searchText: {
    fontFamily: FONTS.OpenSans,
    fontSize: 11,
    alignContent: 'center',
    margin: 0,
  },
  input: {
    alignContent: 'center',
    height: '100%',
  },
});

export default EasySearchBar;
