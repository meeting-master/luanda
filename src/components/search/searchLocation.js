import React, {useCallback, useEffect, useState} from 'react';
import {useFocusEffect} from '@react-navigation/core';
import {FlatList, StyleSheet, TouchableOpacity, View} from 'react-native';
import PropTypes from 'prop-types';
import {ListItem} from 'react-native-elements';
import {useSelector} from 'react-redux';

//services
import {useTranslation} from '../../hooks/useTranslation';
import useRematchDispatch from '../../hooks/useRematchDispatch';
import FONTS from '../../constants/fonts';
import Colors from '../../constants/Colors';

//components
import EasySearchBar from './searchBar';
import {MaterialIcon} from '../Icon';
import CustomModal from '../CustomModal';
import EmptyList from '../emptyList';

const SearchLocation = ({
  onChange,
  locationWithDetail,
  show,
  hasElevation,
  value,
}) => {
  const {t} = useTranslation();
  const {LoadTowns, LoadDistricts} = useRematchDispatch((dispatch) => ({
    LoadTowns: dispatch.location.LoadTowns,
    LoadDistricts: dispatch.location.LoadDistricts,
  }));
  const [searchText, setSearchText] = useState();
  const [showModal, setShowModal] = useState(false);
  const [filterText, setFilterText] = useState();
  const [data, setData] = useState([]);
  const [searchHasFocus, setSearchHasFocus] = useState(false);

  const loadedTowns = useSelector((state) => state.location.towns);
  const loadedDistricts = useSelector((state) => state.location.districts);

  useFocusEffect(
    useCallback(() => {
      if (locationWithDetail) {
        LoadDistricts();
      } else {
        LoadTowns();
      }
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []),
  );

  useEffect(() => {
    setShowModal(show);
  }, [show]);

  useEffect(() => {
    setSearchText(value);
  }, [value]);

  useEffect(() => {
    setData(locationWithDetail ? loadedDistricts : loadedTowns);
  }, [locationWithDetail, loadedTowns, loadedDistricts]);

  const renderIcon = () => (
    <MaterialIcon
      style={styles.icon}
      name="location-on"
      size={25}
      color={Colors.royalblue}
    />
  );

  const modalVisibility = () => setShowModal(!showModal);

  const handleFilterChanger = (filter) => {
    const loadedData = locationWithDetail ? loadedDistricts : loadedTowns;
    const dataToDisplay = loadedData.filter((x) =>
      x.location.toLowerCase().includes(filter.toLowerCase()),
    );
    setData(dataToDisplay);
    setFilterText(filter);
  };

  const renderItem = ({item}) => (
    <TouchableOpacity
      onPress={() => {
        //setSearchHasFocus(false);
        setShowModal(false);
        setSearchText(item.location);
        onChange(item);
      }}>
      <ListItem
        title={item.location}
        titleStyle={styles.listTitle}
        containerStyle={styles.listItem}
        chevron
      />
    </TouchableOpacity>
  );

  const renderX = () => (
    <FlatList
      style={styles.listContainer}
      data={data}
      renderItem={renderItem}
      keyExtractor={(item) => item.id}
      ListEmptyComponent={<EmptyList message={t('search_empty_message')} />}
    />
  );

  const onSearchBarFocus = () => {
    if (!searchHasFocus) {
      setShowModal(true);
      setSearchHasFocus(true);
    } else if (!showModal) {
      setSearchHasFocus(false);
    }
  };

  const onCancel = () => {
    onChange();
  };

  return (
    <>
      <EasySearchBar
        value={searchText}
        onChangeText={setSearchText}
        placeHolder={
          show
            ? t('init_search_location_placeholder')
            : t('search_location_placeholder')
        }
        searchIcon={renderIcon}
        onFocus={onSearchBarFocus}
        onCancel={onCancel}
        hasElevation={hasElevation}
      />
      <CustomModal onCloseModal={modalVisibility} isVisible={showModal}>
        <View style={styles.container}>
          <EasySearchBar
            value={filterText}
            onChangeText={handleFilterChanger}
            onCancel={modalVisibility}
            placeHolder={t('search_location_placeholder')}
          />
          {renderX()}
        </View>
      </CustomModal>
    </>
  );
};

SearchLocation.propTypes = {
  onChange: PropTypes.func.isRequired,
  locationWithDetail: PropTypes.bool,
  show: PropTypes.bool,
  hasElevation: PropTypes.bool,
};

SearchLocation.defaultProps = {
  hasElevation: false,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.greyhish,
  },
  searchText: {
    fontFamily: FONTS.OpenSans,
    fontSize: 13,
  },
  listItem: {
    margin: 2,
  },
  listTitle: {
    fontFamily: FONTS.OpenSans,
    color: Colors.primary,
    fontSize: 13,
    textAlign: 'justify',
  },
  listContainer: {
    marginHorizontal: 5,
  },
});

export default SearchLocation;
