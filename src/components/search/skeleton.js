import React from 'react';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import {View} from 'react-native';

const SearchSkeleton = ({value}) => {
  const item = (v) => {
    return (
      <SkeletonPlaceholder key={v}>
        <SkeletonPlaceholder.Item
          flexDirection="row"
          alignItems="center"
          marginBottom={10}>
          <SkeletonPlaceholder.Item width={60} height={60} borderRadius={50} />
          <SkeletonPlaceholder.Item marginLeft={20}>
            <SkeletonPlaceholder.Item
              width={300}
              height={20}
              borderRadius={4}
            />
            <SkeletonPlaceholder.Item
              marginTop={6}
              width={150}
              height={20}
              borderRadius={4}
            />
          </SkeletonPlaceholder.Item>
        </SkeletonPlaceholder.Item>
      </SkeletonPlaceholder>
    );
  };

  return <View>{new Array(value).fill({}).map((k, v) => item(v))}</View>;
};

export default SearchSkeleton;
