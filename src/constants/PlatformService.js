import {Platform} from 'react-native';

export const isAndroid = () => Platform.OS === 'android';

export const isIos = () => Platform.OS === 'ios';

export const Version = () =>
  isAndroid() ? Platform.Version : parseInt(Platform.Version, 10);
