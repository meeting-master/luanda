import env from 'react-native-config';

const config = {
  API: env.API_HOST,
  VERSION: env.V_NAME,
  DEFAULT_IMAGE_URL: env.DEFAULT_IMAGE_URL,
};

export default config;
