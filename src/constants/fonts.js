const FONTS = {
  SpaceMono: 'SpaceMono-Regular',
  Futura: 'Futura',
  Avenir: 'Avenir',
  OpenSans: 'OpenSans-Regular',
  OpenSansBold: 'OpenSans-Bold',
  OpenSansItalic: 'OpenSans-Italic',
};
export default FONTS;
