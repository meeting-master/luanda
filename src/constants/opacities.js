import React from 'react';

const OPACITIES = {
  lightShadow: 0.1,
  modalBackDrop: 0.3,
  pressedButton: 0.8,
};

export default OPACITIES;
