import {createModel} from '@rematch/core';

import * as queries from '../queries/announce';
import auth from '@react-native-firebase/auth';

export const Announce = () =>
  createModel({
    state: {
      isLoading: false,
      errorMessage: '',
      operationWellDoneMessage: '',
      announceData: [],
      myAnnounces: [],
      announce: null,
    },
    reducers: {
      setLoading(state, isLoading) {
        return {...state, isLoading: isLoading};
      },
      updateAnnounceData(state, data) {
        return {...state, announceData: data, isLoading: false};
      },
      updateMyAnnounces(state, data) {
        return {...state, myAnnounces: data, isLoading: false};
      },
      updateAnnounce(state, data) {
        return {...state, announce: data, isLoading: false};
      },
      setErrorMessage(state, errorMessage) {
        return {
          ...state,
          errorMessage: errorMessage,
          isLoading: false,
        };
      },
      setOperationWellDoneMessage(state, wellDoneMessage) {
        return {
          ...state,
          operationWellDoneMessage: wellDoneMessage,
          isLoading: false,
        };
      },
    },

    effects: (dispatch) => ({
      async clearErrorMessage() {
        this.setErrorMessage('');
      },
      async resetOperationWellDoneMessage() {
        this.setOperationWellDoneMessage('');
      },
      async LoadAnnounces() {
        this.setLoading(true);
        try {
          const result = await queries.FetchLastAnnounces();
          this.updateAnnounceData(
            result.data.response !== null ? result.data.response : [],
          );
        } catch (err) {
          this.setLoading(false);
          this.updateAnnounceData([]);
        }
      },
      async LoadAnnouncesByTag(payload) {
        this.setLoading(true);
        try {
          const result = await queries.FetchAnnouncesByTag(payload);
          this.updateAnnounceData(
            result.data.response !== null ? result.data.response : [],
          );
        } catch (err) {
          this.setLoading(false);
          this.updateAnnounceData([]);
        }
      },
      async LoadAnnouncesByOwner(payload) {
        this.setLoading(true);
        try {
          const result = await queries.FetchAnnouncesByOwner(payload);
          this.updateMyAnnounces(
            result.data.response !== null ? result.data.response : [],
          );
        } catch (err) {
          this.setLoading(false);
          this.updateAnnounceData([]);
        }
      },
      async GetAnnounceById(payload) {
        this.setLoading(true);
        this.ClearAnnounce();
        try {
          const result = await queries.GetAnnounce(payload);
          this.updateAnnounce(
            result.data.response !== null ? result.data.response : null,
          );
        } catch (err) {
          this.setLoading(false);
          this.updateAnnounce(null);
        }
      },
      async ClearAnnounce() {
        this.updateAnnounce(null);
      },
      async CreateAnnounce(payload) {
        this.setLoading(true);
        this.setErrorMessage('');
        this.setOperationWellDoneMessage('');
        var user = auth().currentUser;
        if (user.emailVerified) {
          try {
            await queries.CreateAnnounce(payload);
            this.setOperationWellDoneMessage('common.operationWellDone');
          } catch (err) {
            this.setErrorMessage(err);
            this.setLoading(false);
          }
        } else {
          this.setErrorMessage('auth/unvalidated-email');
        }
      },
      async DeleteAnnounce(payload) {
        this.setLoading(true);
        this.setErrorMessage('');
        this.setOperationWellDoneMessage('');
        try {
          const result = await queries.DeleteAnnounce(payload);
          this.setOperationWellDoneMessage('common.operationWellDone');
        } catch (err) {
          this.setErrorMessage(err);
          console.log(err);
          this.setLoading(false);
        }
      },
      async UpdateAnnounce(payload) {
        this.setLoading(true);
        this.setErrorMessage('');
        this.setOperationWellDoneMessage('');
        try {
          const result = await queries.UpdateAnnounce(payload);
          this.setOperationWellDoneMessage('common.operationWellDone');
        } catch (err) {
          this.setErrorMessage(err);
          console.log(err);
          this.setLoading(false);
        }
      },
    }),
  });
