import {createModel} from '@rematch/core';
import * as Request from '../queries/calendar';
import auth from '@react-native-firebase/auth';

export const CalendarModel = () =>
  createModel({
    state: {
      ongoing: false,
      error: null,
      serviceCalendar: [],
      operationState: null,
      dateWithAvailableTimeSlots: [],
    },
    reducers: {
      setErrors(state, err) {
        return {...state, error: err};
      },
      updateOngoing(state, ok) {
        return {...state, ongoing: ok};
      },
      updateCalendar(state, sc) {
        return {...state, ongoing: false, serviceCalendar: sc};
      },
      updateOperationState(state, op) {
        return {...state, operationState: op};
      },
      updateDateWithAvailableTimeSlots(state, availableDate) {
        return {...state, dateWithAvailableTimeSlots: availableDate};
      },
    },

    effects: (dispatch) => ({
      async CreateAppointment(payload) {
        this.updateOperationState(null);
        let user = auth().currentUser;
        if (user.emailVerified) {
          this.updateOngoing(true);
          this.setErrors(null);
          try {
            await Request.CreateAppointment(payload);
            this.updateOperationState(true);
            this.updateCalendar([]);
          } catch (err) {
            this.updateOperationState(false);
          } finally {
            this.updateOngoing(false);
          }
        } else {
          this.setErrors('auth/unvalidated-email');
          this.updateOperationState(false);
        }
      },

      async GetServiceCalendar(payload) {
        this.updateOperationState(null);
        this.updateOngoing(true);
        try {
          const result = await Request.GetServiceCalendar(payload);
          const availableDate =
            result.data.response !== null
              ? result.data.response
                  .filter(
                    (sc) => sc.isAvailable && new Date(sc.date) >= new Date(),
                  )
                  .map((sc) => sc.date)
              : [];
          this.updateDateWithAvailableTimeSlots(availableDate);
          this.updateCalendar(
            result.data.response !== null ? result.data.response : [],
          );
        } catch (err) {
          this.updateOngoing(false);
        }
      },
    }),
  });
