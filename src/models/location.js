import {createModel} from '@rematch/core';

import * as Request from '../queries/location';

export const Location = () =>
  createModel({
    state: {
      countries: [],
      towns: [],
      districts: [],
    },
    reducers: {
      updateCountries(state, countries) {
        return {...state, countries};
      },
      updateTowns(state, towns) {
        return {...state, towns};
      },
      updateDistricts(state, districts) {
        return {...state, districts};
      },
    },

    effects: (dispatch) => ({
      async LoadCountries() {
        try {
          const result = await Request.FetchCountries();
          this.updateCountries(
            result.data.response !== null ? result.data.response : [],
          );
        } catch (err) {}
      },

      async LoadTowns() {
        try {
          const result = await Request.FetchTowns();
          this.updateTowns(
            result.data.response !== null ? result.data.response : [],
          );
        } catch (err) {
          console.log(err);
        }
      },

      async LoadDistricts() {
        try {
          const result = await Request.FetchDistricts();
          this.updateDistricts(
            result.data.response !== null ? result.data.response : [],
          );
        } catch (err) {}
      },
    }),
  });
