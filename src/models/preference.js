import {createModel} from '@rematch/core';
import AsyncStorage from '@react-native-community/async-storage';

export const Preference = () =>
  createModel({
    state: {
      header: {},
      backTo: null,
      navigator: {announce: false, appointment: false, account: false},
    },
    reducers: {
      updateHeader(state, h) {
        return {...state, header: h};
      },

      updateAppointmentLocation(state, location) {
        return {...state, appointmentLocation: location};
      },

      updateAnnounceLocation(state, location) {
        return {...state, announceLocation: location};
      },

      updateBackTo(state, backTo) {
        return {...state, backTo: backTo};
      },

      updateNavigator(state, nav) {
        return {...state, navigator: nav};
      },
    },

    effects: (dispatch) => ({
      SetHeader(header) {
        this.updateHeader(header);
      },

      SetNavigator(nav) {
        this.updateNavigator(nav);
      },

      async SetAnnounceLocation(location) {
        try {
          await AsyncStorage.setItem(
            'announceLocation',
            JSON.stringify(location),
          );
          this.updateAnnounceLocation(location);
        } catch (e) {}
      },

      async SetAppointmentLocation(location) {
        try {
          await AsyncStorage.setItem(
            'appointmentLocation',
            JSON.stringify(location),
          );
          this.updateAppointmentLocation(location);
        } catch (e) {}
      },

      async LoadAnnounceLocation() {
        try {
          const value = await AsyncStorage.getItem('announceLocation');
          if (value !== null) {
            await this.updateAnnounceLocation(JSON.parse(value));
          } else {
            await this.updateAnnounceLocation(null);
          }
        } catch (e) {}
      },

      async LoadAppointmentLocation() {
        try {
          const value = await AsyncStorage.getItem('appointmentLocation');
          if (value !== null) {
            await this.updateAppointmentLocation(JSON.parse(value));
          } else {
            await this.updateAppointmentLocation(null);
          }
        } catch (e) {}
      },

      async SetBackTo(backTo) {
        this.updateBackTo(backTo);
      },
      async ClearBackTo() {
        this.updateBackTo(null);
      },
    }),
  });
