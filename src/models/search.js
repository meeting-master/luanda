import {createModel} from '@rematch/core';

import * as Request from '../queries/search';

export const Search = () =>
  createModel({
    state: {
      searchData: [],
      isLoading: false,
      officeServices: [],
      office: null,
      ratingComments: [],
    },
    reducers: {
      updateLoading(state, loading) {
        return {...state, isLoading: loading};
      },
      updateOfficeServices(state, data) {
        return {...state, officeServices: data, isLoading: false};
      },
      updateSearch(state, data) {
        return {...state, searchData: data, isLoading: false};
      },
      updateOffice(state, data) {
        return {...state, office: data};
      },
      updateOfficeComments(state, data) {
        return {...state, ratingComments: data};
      },
    },

    effects: (dispatch) => ({
      async LoadInitial() {
        this.updateLoading(true);
        try {
          const result = await Request.GetTopTen();
          this.updateSearch(
            result.data.response !== null ? result.data.response : [],
          );
        } catch (err) {
          this.updateLoading(false);
          this.updateSearch([]);
        }
      },
      async LoadSearchData(payload) {
        this.updateLoading(true);
        try {
          const result = await Request.FetchAccountData(payload);
          this.updateSearch(
            result.data.response !== null ? result.data.response : [],
          );
        } catch (err) {
          this.updateLoading(false);
          this.updateOfficeServices([]);
        }
      },
      async LoadOfficeServices(officeId) {
        this.updateLoading(true);
        this.updateOfficeServices([]);
        try {
          const result = await Request.FetchOfficeService(officeId);
          this.updateOfficeServices(
            result.data.response !== null ? result.data.response : [],
          );
        } catch (err) {
          this.updateLoading(false);
          this.updateOfficeServices([]);
        }
      },
      async LoadOfficeRatingComments(officeId) {
        this.updateLoading(true);
        this.updateOfficeComments([]);
        try {
          const result = await Request.FetchOfficeRating(officeId);
          this.updateOfficeComments(
            result.data.response !== null ? result.data.response : [],
          );
        } catch (err) {
          this.updateLoading(false);
          this.updateOfficeComments([]);
        }
      },
      async setOffice(office) {
        this.updateOffice(office);
      },
    }),
  });
