import {init} from '@rematch/core';
import {User} from './user';
import {Location} from './location';
import {Search} from './search';
import {Preference} from './preference';
import {CalendarModel} from './calendar';
import {Announce} from './announce';

export const initModels = () => {
  return {
    user: User(),
    calendar: CalendarModel(),
    location: Location(),
    preference: Preference(),
    search: Search(),
    announce: Announce(),
  };
};

export const initStore = () =>
  init({
    models: initModels(),
  });
