import {createModel} from '@rematch/core';
import auth from '@react-native-firebase/auth';

import * as queries from '../queries/user';
import {getLanguage} from '../translation/lang';

export const User = () =>
  createModel({
    state: {
      registerUser: null,
      currentUser: null,
      errorMessage: '',
      isLoading: false,
      historyData: [],
      operationWellDoneMessage: '',
    },
    reducers: {
      setLoading(state, isLoading) {
        return {...state, isLoading: isLoading};
      },
      setCurrentUser(state, user) {
        return {
          ...state,
          currentUser: user,
          isLoading: false,
        };
      },
      setRegisterUser(state, registerUser) {
        return {
          ...state,
          registerUser: registerUser,
          isLoading: false,
        };
      },
      updateHistoryData(state, data) {
        return {...state, historyData: data, isLoading: false};
      },
      setErrorMessage(state, errorMessage) {
        return {
          ...state,
          errorMessage: errorMessage,
          isLoading: false,
        };
      },
      setOperationWellDoneMessage(state, wellDoneMessage) {
        return {
          ...state,
          operationWellDoneMessage: wellDoneMessage,
          isLoading: false,
        };
      },
    },

    effects: (dispatch) => ({
      async Register(payload) {
        this.setRegisterUser(null);
        this.setErrorMessage('');
        this.setLoading(true);
        auth().languageCode = getLanguage();
        auth()
          .createUserWithEmailAndPassword(payload.email, payload.password)
          .then((res) => {
            res.user.sendEmailVerification();
            this.setRegisterUser(res.user);
          })
          .catch((error) => {
            this.setErrorMessage(error.code);
          });
      },
      async UpdateAccount(payload) {
        this.setErrorMessage('');
        this.setLoading(true);
        let user = auth().currentUser;

        try {
          await queries.SetUserInfo({id: user.uid, detail: JSON.stringify(payload)});

          user
            .updateProfile({
              displayName: `${payload.lastName} ${payload.firstName}`,
            })
            .then(async () => {
              this.setCurrentUser(
                await augmentCurrentUserWithDBInfo(auth().currentUser),
              );
            })
            .catch(function (error) {
              this.setErrorMessage(error.code);
            });
        } catch (err) {
          this.setErrorMessage(err);
          this.setLoading(false);
        }
      },
      async login(payload) {
        this.setCurrentUser(null);
        this.setErrorMessage('');
        this.setLoading(true);
        auth()
          .signInWithEmailAndPassword(payload.email, payload.password)
          .then(async (res) => {
            if (res.user.emailVerified) {
              this.setCurrentUser(await augmentCurrentUserWithDBInfo(res.user));
            } else {
              this.setErrorMessage('auth/email-not-verified')
            }
          })
          .catch((error) => {
            this.setErrorMessage(error.code);
          });
      },
      async CheckActiveSession() {
        let user = auth().currentUser;
        if (user !== null) {
          this.setCurrentUser(await augmentCurrentUserWithDBInfo(user));
        }
      },
      async logout() {
        this.setLoading(true);
        auth()
          .signOut()
          .then(() => {
            console.log('User signed out!');
            this.setCurrentUser(null);
            this.setRegisterUser(null);
          })
          .catch((error) => {
            console.log(error);
            this.setErrorMessage(error.code);
          });
      },
      async updateEmail(payload) {
        this.setRegisterUser(null);
        this.setErrorMessage('');
        this.setLoading(true);
        auth().languageCode = getLanguage();
        let user = auth().currentUser;
        let cred = auth.EmailAuthProvider.credential(
          user.email,
          payload.password,
        );
        user
          .reauthenticateWithCredential(cred)
          .then(() => {
            user
              .updateEmail(payload.email)
              .then(async () => {
                console.log('Email updated successfully!');
                const cu = await augmentCurrentUserWithDBInfo(auth().currentUser)
                this.setCurrentUser(cu);
                this.setRegisterUser(cu);
              })
              .catch((error) => {
                console.log(error);
                this.setErrorMessage(error.code);
              });
          })
          .catch((error) => {
            console.log(error);
            this.setErrorMessage(error.code);
          });
      },
      async updatePassword(payload) {
        this.setRegisterUser(null);
        this.setErrorMessage('');
        this.setLoading(true);
        let user = auth().currentUser;
        let cred = auth.EmailAuthProvider.credential(
          user.email,
          payload.oldPassword,
        );
        user
          .reauthenticateWithCredential(cred)
          .then(() => {
            user
              .updatePassword(payload.password)
              .then(async () => {
                const cu = await augmentCurrentUserWithDBInfo(auth().currentUser)
                this.setCurrentUser(cu);
                this.setRegisterUser(cu);
              })
              .catch((error) => {
                this.setErrorMessage(error.code);
              });
          })
          .catch((error) => {
            this.setErrorMessage(error.code);
          });
      },
      async resetPassword(payload) {
        this.setCurrentUser(null);
        this.setErrorMessage('');
        this.setOperationWellDoneMessage('');
        this.setLoading(true);
        auth().languageCode = getLanguage();
        auth()
          .sendPasswordResetEmail(payload.email)
          .then(() => {
            this.setOperationWellDoneMessage(
              'loginScreen.successful_reset_password_request',
            );
          })
          .catch((error) => {
            this.setErrorMessage(error.code);
          });
      },
      async clearErrorMessage() {
        this.setErrorMessage('');
      },
      async clearRegisterUser() {
        this.setRegisterUser(null);
      },
      async resetOperationWellDoneMessage() {
        this.setOperationWellDoneMessage('');
      },
      async deleteHistory(payload) {
        this.setLoading(true);
        this.setErrorMessage('');
        this.setOperationWellDoneMessage('');
        try {
          await queries.DeleteHistory(payload.data);
          this.setOperationWellDoneMessage('historyScreen.successful_delete');
        } catch (err) {
          this.setErrorMessage(err);
          this.setLoading(false);
        }
      },
      async rateAndComment(payload) {
        this.setLoading(true);
        this.setErrorMessage('');
        this.setOperationWellDoneMessage('');
        try {
          await queries.rateAndComment(payload);
          this.setOperationWellDoneMessage('common.operationWellDone');
        } catch (err) {
          this.setErrorMessage(err);
          this.setLoading(false);
        }
      },
      async LoadHistory(payload) {
        this.setLoading(true);
        try {
          const result = await queries.FetchHistory(payload);
          this.updateHistoryData(
            result.data.response !== null ? result.data.response : [],
          );
        } catch (err) {
          this.setLoading(false);
          this.updateHistoryData([]);
        }
      },
      async SetFCMToken(payload) {
        try {
          await queries.SetToken({
            userId: auth().currentUser.uid,
            token: payload,
            lang: getLanguage(),
          });
        } catch (err) {}
      },
    }),
  });

const augmentCurrentUserWithDBInfo = async (cu) => {
    const { uid, photoURL, providerId, emailVerified, email, isAnonymous, displayName } = cu;
    var newCu = { uid, photoURL, providerId, emailVerified, email, isAnonymous, displayName };
    var userInfo = ''
    try {
      const result = await queries.FetchUserInfo(cu.uid);
      userInfo = result.data.response !== null && result.data.response.detail !== '' ? result.data.response.detail : '{}'
    } catch (err) {

    }
    return {...newCu, displayName: userInfo}
  }
