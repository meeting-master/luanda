import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import SettingsScreen from '../screens/account/SettingsScreen';
import AnnounceHistoryScreen from '../screens/account/AnnounceHistoryScreen';
import AppointmentHistoryScreen from '../screens/account/AppointmentHistoryScreen';

import ROUTES from './Routes';

const Stack = createStackNavigator();

const AccountNavigator = () => {
  return (
    <Stack.Navigator initialRouteName={ROUTES.account.settings}>
      <Stack.Screen name={ROUTES.account.settings} component={SettingsScreen} />
      <Stack.Screen
        name={ROUTES.account.announceHistory}
        component={AnnounceHistoryScreen}
      />
      <Stack.Screen
        name={ROUTES.account.appointmentHistory}
        component={AppointmentHistoryScreen}
      />
    </Stack.Navigator>
  );
};
export default AccountNavigator;
