import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import AnnounceScreen from '../screens/announce/AnnounceScreen';

const Stack = createStackNavigator();

const AnnounceNavigator = (props) => {
  return (
    <Stack.Navigator initialRouteName="AnnounceScreen">
      <Stack.Screen name="AnnounceScreen" component={AnnounceScreen} />
    </Stack.Navigator>
  );
};
export default AnnounceNavigator;
