import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import SearchScreen from '../screens/appointment/SearchScreen';
import ServiceScreen from '../screens/appointment/ServiceScreen';
import AppointmentScreen from '../screens/appointment/AppointmentScreen';
import ConfirmationScreen from '../screens/appointment/ConfirmationScreen';
import ROUTES from './Routes';

const Stack = createStackNavigator();

const AppointmentNavigator = () => {
  return (
    <Stack.Navigator initialRouteName={ROUTES.appointmentNavigation.search}>
      <Stack.Screen
        name={ROUTES.appointmentNavigation.search}
        component={SearchScreen}
      />
      <Stack.Screen
        name={ROUTES.appointmentNavigation.summary}
        component={ServiceScreen}
      />
      <Stack.Screen
        name={ROUTES.appointmentNavigation.appointment}
        component={AppointmentScreen}
      />
      <Stack.Screen
        name={ROUTES.appointmentNavigation.confirmation}
        component={ConfirmationScreen}
      />
    </Stack.Navigator>
  );
};
export default AppointmentNavigator;
