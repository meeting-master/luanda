import * as React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

//hooks
import {useTranslation} from '../hooks/useTranslation';

//services
import Colors from '../constants/Colors';
import FONTS from '../constants/fonts';

//components
import {MaterialIcon} from '../components/Icon';
import AnnounceNavigator from './AnnounceNavigator';
import AppointmentNavigator from './AppointmentNavigator';
import useRematchDispatch from '../hooks/useRematchDispatch';
import {useEffect} from 'react';
import LinearGradient from 'react-native-linear-gradient';
import ROUTES from './Routes';
import AccountNavigator from './AccountNavigator';

const BottomTab = createBottomTabNavigator();

export default function BottomTabNavigator({navigation, route}) {
  const {t} = useTranslation();
  const {
    LoadAnnounceLocation,
    LoadAppointmentLocation,
    SetNavigator,
  } = useRematchDispatch((dispatch) => ({
    LoadAnnounceLocation: dispatch.preference.LoadAnnounceLocation,
    LoadAppointmentLocation: dispatch.preference.LoadAppointmentLocation,
    SetNavigator: dispatch.preference.SetNavigator,
  }));

  useEffect(() => {
    LoadAnnounceLocation();
    LoadAppointmentLocation();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const tabBar = (focused, icon, label) => {
    if (focused) {
      return (
        <LinearGradient
          colors={[Colors.royalblue, Colors.lightblue]}
          style={styles.container}
          start={{x: 0, y: 0}}
          end={{x: 1, y: 0}}>
          <MaterialIcon
            color={Colors.white}
            name={icon}
            size={20}
            style={{marginTop: 8}}
          />
          <Text style={[styles.label, {color: Colors.white}]}>{label}</Text>
        </LinearGradient>
      );
    } else {
      return (
        <View style={styles.container}>
          <MaterialIcon
            color={Colors.royalblue}
            name={icon}
            size={20}
            style={{marginTop: 8}}
          />
          <Text style={[styles.label, {color: Colors.royalblue}]}>{label}</Text>
        </View>
      );
    }
  };

  return (
    <BottomTab.Navigator
      initialRouteName={ROUTES.tabNavigator.appointment}
      tabBarOptions={{
        inactiveTintColor: Colors.meanBlue,
        labelStyle: {fontFamily: FONTS.OpenSans},
        showLabel: false,
      }}>
      <BottomTab.Screen
        name={ROUTES.tabNavigator.announce}
        component={AnnounceNavigator}
        options={{
          title: t('title_announce'),
          tabBarIcon: ({focused}) =>
            tabBar(focused, 'event-note', t('title_announce')),
        }}
      />
      <BottomTab.Screen
        name={ROUTES.tabNavigator.appointment}
        component={AppointmentNavigator}
        options={{
          title: t('title_search'),
          tabBarIcon: ({focused}) =>
            tabBar(focused, 'timer', t('title_appointment')),
        }}
        listeners={({navigation, route}) => ({
          tabPress: (e) => {
            SetNavigator({announce: false, appointment: true, account: false});
          },
        })}
      />
      <BottomTab.Screen
        name={ROUTES.tabNavigator.account}
        component={AccountNavigator}
        options={{
          title: t('title_account'),
          tabBarIcon: ({focused}) =>
            tabBar(focused, 'person-outline', t('title_account')),
        }}
        listeners={({navigation, route}) => ({
          tabPress: (e) => {
            SetNavigator({announce: false, appointment: false, account: true});
          },
        })}
      />
    </BottomTab.Navigator>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    height: '100%',
    alignItems: 'center',
  },
  label: {
    fontFamily: FONTS.OpenSans,
    fontSize: 11,
  },
});
