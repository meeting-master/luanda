import React, {useCallback, useEffect} from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {useSelector} from 'react-redux';
import {useFocusEffect} from '@react-navigation/core';

import HistoryScreen from '../screens/account/AppointmentHistoryScreen';
import RatingScreen from '../screens/account/RatingScreen';

//hooks
import useRematchDispatch from '../hooks/useRematchDispatch';

const Stack = createStackNavigator();

const HistoryNavigator = (props) => {
  const currentUser = useSelector((state) => state.user.currentUser);
  const {clearErrorMessage, resetOperationWellDone} = useRematchDispatch(
    (dispatch) => ({
      clearErrorMessage: dispatch.user.clearErrorMessage,
      resetOperationWellDone: dispatch.user.resetOperationWellDone,
    }),
  );

  useFocusEffect(
    useCallback(() => {
      if (currentUser === null) {
        props.navigation.navigate('login', {backTo: 'HistoryNavigator'});
      }
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [currentUser]),
  );
  return (
    <Stack.Navigator initialRouteName="history">
      <Stack.Screen name="history" component={HistoryScreen} />
      <Stack.Screen name="rating" component={RatingScreen} />
    </Stack.Navigator>
  );
};
export default HistoryNavigator;
