const ROUTES = {
  announce: 'announce',
  root: 'root',
  register: 'register',
  login: 'login',
  profile: 'profile',
  email: 'email',
  help: 'help',
  about: 'about',
  cgu: 'cgu',
  password: 'changePassword',
  phoneNumber: 'phoneNumber',
  account: {
    settings: 'settings',
    announceHistory: 'announceHistory',
    appointmentHistory: 'appointmentHistory',
    rating: 'rating',
  },
  appointmentNavigation: {
    search: 'search',
    summary: 'summary',
    appointment: 'appointment',
    confirmation: 'confirmation',
  },
  tabNavigator: {
    announce: 'announceNavigator',
    appointment: 'appointmentNavigator',
    account: 'accountNavigator',
  },
};

export default ROUTES;
