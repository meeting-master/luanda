import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {useSelector} from 'react-redux';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

//screens
import BottomTabNavigator from './BottomTabNavigator';
import LoginScreen from '../screens/settings/LoginScreen';
import RegisterScreen from '../screens/settings/RegisterScreen';
import SettingsScreen from '../screens/account/SettingsScreen';
import PasswordScreen from '../screens/account/PasswordScreen';
import PhoneNumberScreen from '../screens/account/PhoneNumberScreen';
import ProfileScreen from '../screens/account/ProfileScreen';
import UpdateEmailScreen from '../screens/account/UpdateEmailScreen';
import AboutScreen from '../screens/settings/AboutScreen';
import HelpScreen from '../screens/settings/HelpScreen';
import AppointmentHistoryScreen from '../screens/account/AppointmentHistoryScreen';
import AnnounceHistoryScreen from '../screens/account/AnnounceHistoryScreen';
import RatingScreen from '../screens/account/RatingScreen';
import CGUScreen from '../screens/settings/CGUScreen';

//component
import {MaterialIcon} from '../components/Icon';

//services
import {useTranslation} from '../hooks/useTranslation';
import FONTS from '../constants/fonts';
import ROUTES from './Routes';
import Colors from '../constants/Colors';
import logoText from '../assets/images/logoText.png';

const Stack = createStackNavigator();

const headerStyle = {
  fontFamily: FONTS.OpenSansBold,
  fontSize: 15,
};

const StackNavigator = () => {
  const {t} = useTranslation();
  const header = useSelector((state) => state.preference.header);
  const rightHeader = () => {
    return <Image source={logoText} style={styles.logo} />;
  };
  const leftHeader = (props) => {
    return (
      <View style={styles.leftContainer}>
        {header.backTo ? (
          <TouchableOpacity onPress={() => header.backTo()}>
            <MaterialIcon
              name={'arrow-back'}
              size={20}
              color={Colors.white}
              style={styles.logo}
            />
          </TouchableOpacity>
        ) : (
          <MaterialIcon
            name={header.image}
            size={20}
            color={Colors.white}
            style={styles.logo}
          />
        )}
        <Text style={styles.title}>{header.title}</Text>
      </View>
    );
  };

  const Gradient = () => (
    <LinearGradient
      colors={[Colors.royalblue, Colors.lightblue]}
      style={{flex: 1}}
      start={{x: 0, y: 0}}
      end={{x: 1, y: 0}}
    />
  );

  return (
    <Stack.Navigator
      headerMode={'screen'}
      screenOptions={{
        headerBackground: () => Gradient(),
        headerTintColor: Colors.white,
        headerTitle: null,
        headerRight: () => rightHeader(),
        headerLeft: (props) => leftHeader(props),
      }}>
      <Stack.Screen name={ROUTES.root} component={BottomTabNavigator} />
      <Stack.Screen
        name={ROUTES.register}
        component={RegisterScreen}
        options={{
          title: t('setting_login'),
          headerTitleAlign: 'center',
          headerTitleStyle: headerStyle,
        }}
      />
      <Stack.Screen
        name={ROUTES.login}
        component={LoginScreen}
        options={{
          title: t('setting_login'),
          headerTitleAlign: 'center',
          headerTitleStyle: headerStyle,
        }}
      />
      <Stack.Screen
        name={ROUTES.profile}
        component={ProfileScreen}
        options={{
          title: t('profileScreen.title'),
          headerTitleAlign: 'center',
          headerTitleStyle: headerStyle,
        }}
      />
      <Stack.Screen
        name={ROUTES.email}
        component={UpdateEmailScreen}
        options={{
          title: t('setting_email'),
          headerTitleAlign: 'center',
          headerTitleStyle: headerStyle,
        }}
      />
      <Stack.Screen
        name={ROUTES.password}
        component={PasswordScreen}
        options={{
          title: t('setting_change_password'),
          headerTitleAlign: 'center',
          headerTitleStyle: headerStyle,
        }}
      />
      <Stack.Screen
        name={ROUTES.account.announceHistory}
        component={AnnounceHistoryScreen}
        options={{
          title: t('profileScreen.announces'),
          headerTitleAlign: 'center',
          headerTitleStyle: headerStyle,
          headerShown: false,
        }}
      />
      <Stack.Screen
        name={ROUTES.account.rating}
        component={RatingScreen}
        options={{
          title: t('rating'),
          headerTitleAlign: 'center',
          headerTitleStyle: headerStyle,
        }}
      />
      <Stack.Screen
        name={ROUTES.account.appointmentHistory}
        component={AppointmentHistoryScreen}
        options={{
          title: t('profileScreen.appointments'),
          headerTitleAlign: 'center',
          headerTitleStyle: headerStyle,
        }}
      />
      <Stack.Screen
        name={ROUTES.account.settings}
        component={SettingsScreen}
        options={{title: t('label_setting')}}
      />
      <Stack.Screen
        name={ROUTES.help}
        component={HelpScreen}
        options={{title: t('setting_help_title')}}
      />
      <Stack.Screen
        name={ROUTES.about}
        component={AboutScreen}
        options={{title: t('setting_about')}}
      />
      <Stack.Screen
        name={ROUTES.cgu}
        component={CGUScreen}
        options={{title: t('cgu')}}
      />
      <Stack.Screen
        name={ROUTES.phoneNumber}
        component={PhoneNumberScreen}
        options={{title: t('setting_phone_number')}}
      />
    </Stack.Navigator>
  );
};

const styles = StyleSheet.create({
  leftContainer: {
    flexDirection: 'row',
    alignContent: 'center',
    paddingTop: 4,
  },
  title: {
    color: Colors.white,
    fontFamily: FONTS.OpenSans,
  },

  logo: {
    marginHorizontal: 10,
    justifyContent: 'center',
  },
});
export default StackNavigator;
