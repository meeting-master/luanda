import axios from './axios';
const BASE = '/announce';

export const CreateAnnounce = async (data) => {
  const config = {
    headers: {
      'Content-Type': 'multipart/form-data',
    },
  };
  return axios.post(BASE, data, config);
};

export const FetchAnnouncesByTag = async (data) => {
  return axios.get(BASE + 's-by-tag', {
    params: {
      tag: data.tag,
      townId: data.townId,
    },
  });
};

export async function FetchLastAnnounces() {
  return axios.get('/last-announces');
}

export const FetchAnnouncesByOwner = async (id) => {
  return axios.get(BASE + 's-by-owner', {
    params: {
      ownerId: id,
    },
  });
};

export const GetAnnounce = async (data) => {
  return axios.get(BASE, {
    params: {
      id: data.id,
    },
  });
};

export async function DeleteAnnounce(data) {
  return axios.delete(BASE, {
    params: {data},
  });
}

export const UpdateAnnounce = async (data) => {
  return axios.put(BASE, data);
};
