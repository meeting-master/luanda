import axios from 'axios';
import config from '../constants/config';

axios.defaults.baseURL = config.API;

/*
axios.interceptors.request.use(
    config => {
        config.headers.authorization = `Bearer ${localStorage.getItem("Token")}`;
        return config
    },
    error => {
        return Promise.reject(error);
    }
);
*/
export default axios;
