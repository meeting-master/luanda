import axios from './axios';

const BASE = '/calendar';

export const GetServiceCalendar = async (data) => {
  return axios.post(BASE + '/calendar', data);
};

export const CreateAppointment = async (data) => {
  return axios.post(BASE + '/appointment', data);
};
