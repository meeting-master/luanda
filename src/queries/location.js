import axios from './axios';

const BASE = '/location';

export const FetchCountries = async () => {
  return axios.get(BASE + '/countries');
};

export const FetchTowns = async () => {
  return axios.get(BASE + '/locations-by-town');
};

export const FetchDistricts = async () => {
  return axios.get(BASE + '/locations-by-district');
};
