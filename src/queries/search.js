import axios from './axios';

const BASE = '/customer';

export const GetTopTen = async () => {
  return axios.get(BASE + '/office-by-tag', {
    params: {
      tag: '',
    },
  });
};

export const FetchAccountData = async (data) => {
  return axios.get(BASE + '/office-by-tag', {
    params: {
      tag: data.tag,
      townId: data.townId,
      lat: data.lat,
      lon: data.lon,
    },
  });
};

export const FetchOfficeService = async (officeId) => {
  return axios.get(BASE + '/office-services', {
    params: {
      officeId,
    },
  });
};

export const FetchOfficeRating = async (officeId) => {
  return axios.get(BASE + '/office-rating', {
    params: {
      officeId,
    },
  });
};
