import axios from './axios';
const API = '';
export function fetchCountries() {
  return new Promise((callBack) => {
    axios
      .get(API + '/position/countries')
      .then((result) => callBack(result))
      .catch((error) => console.log(error));
  });
}

export function FetchHistory(id) {
  return axios.get('calendar/appointment-history', {
    params: {
      id,
    },
  });
}

export function DeleteHistory(data) {
  return axios.delete('calendar/appointment', {
    params: {
      data,
    },
  });
}
export const rateAndComment = async (data) => {
  return axios.post('calendar/rating', data);
};

export const SetToken = async (data) => {
  return axios.post('calendar/device-token', data);
};

export function FetchUserInfo(id) {
  return axios.get('user/end-user', {
    params: {
      id,
    },
  });
}

export const SetUserInfo = async (data) => {
  return axios.post('user/end-user', data);
};
