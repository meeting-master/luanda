import React, {useEffect} from 'react';
import {Image, Text, View, ScrollView} from 'react-native';

import C19Button from '../components/C19Button';
import C19Styles from '../constants/C19Styles';

import useRematchDispatch from '../hooks/useRematchDispatch';

export default function ContactScreen() {
  return (
    <View style={C19Styles.container}>
      <ScrollView
        style={C19Styles.container}
        contentContainerStyle={C19Styles.contentContainer}
      />
    </View>
  );
}

ContactScreen.navigationOptions = {
  header: null,
};
