import React, {useEffect, useCallback} from 'react';
import {
  StyleSheet,
  SafeAreaView,
  Alert,
  View,
  Text,
  ActivityIndicator,
} from 'react-native';

import {useTranslation} from '../hooks/useTranslation';
import {useFocusEffect} from '@react-navigation/core';
import useRematchDispatch from '../hooks/useRematchDispatch';
import {useSelector} from 'react-redux';
import Colors from '../constants/Colors';
import format from 'date-fns/format';
import {Button} from 'react-native-elements';
import FONTS from '../constants/fonts';

export default function HomeScreen() {
  const {t} = useTranslation();
  const {SetHeader} = useRematchDispatch((dispatch) => ({
    SetHeader: dispatch.preference.SetHeaderTitle,
  }));

  useFocusEffect(() => {
    SetHeader(t('label_home'));
  }, []);

  const renderX = (left, right, style) => (
    <View style={styles.item}>
      <View style={styles.leftContainer}>
        <Text style={[styles.leftText, style]}>{left}</Text>
      </View>
      <View style={styles.rightContainer}>
        <Text style={[styles.rightText, style]}>{right}</Text>
      </View>
    </View>
  );
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.header}>
        <Text style={styles.button}>JOURNALIER</Text>
      </View>
      <View style={styles.body}>
        {renderX(t('new_cases'), 45, {color: 'darkred'})}
        {renderX(t('new_healed'), 127, {color: 'darkgreen'})}
        {renderX(t('new_deaths'), 2, {color: 'black'})}
      </View>
      <View style={styles.header}>
        <Text style={styles.button}>GLOBAL</Text>
      </View>
      <View style={styles.body}>
        {renderX(t('total_cases'), 8270)}
        {renderX(t('total_healed'), 6404, {color: 'darkgreen'})}
        {renderX(t('total_deaths'), 53, {color: 'darkred'})}
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  body: {
    margin: 10,
    backgroundColor: Colors.white,
    borderRadius: 5,
    shadowColor: Colors.black,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    alignSelf: 'baseline',
    width: '95%',
  },
  item: {
    padding: 15,
    left: 0,
    width: '100%',
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderColor: Colors.light,
    marginBottom: 10,
    flexDirection: 'row',
  },
  leftContainer: {
    width: '40%',
  },
  leftText: {
    fontFamily: FONTS.OpenSans,
    fontSize: 12,
    color: Colors.dark,
  },
  rightContainer: {
    width: '60%',
    alignItems: 'flex-end',
    marginLeft: 5,
  },
  rightText: {
    fontFamily: FONTS.Avenir,
    fontSize: 11,
    color: Colors.primary,
    textAlign: 'right',
  },
  button: {
    fontFamily: FONTS.OpenSans,
    fontSize: 14,
    color: Colors.primary,
  },
  header: {
    marginVertical: 10,
  },
});
