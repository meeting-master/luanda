import React, {useCallback, useEffect, useState} from 'react';
import {
  FlatList,
  Image,
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  View,
  TextInput,
  Platform,
  KeyboardAvoidingView,
  Text,
} from 'react-native';
import {useFocusEffect} from '@react-navigation/core';
import {useSelector} from 'react-redux';
import FormData from 'form-data';

//hooks
import {useTranslation} from '../../hooks/useTranslation';
import useRematchDispatch from '../../hooks/useRematchDispatch';

//Services
import Colors from '../../constants/Colors';
import logoText from '../../assets/images/logoText.png';
import FONTS from '../../constants/fonts';

//components

import HeaderBar from '../../components/HeaderBar';
import {IonIcon, MaterialIcon} from '../../components/Icon';
import * as ImagePicker from 'react-native-image-picker';
import SearchLocation from '../../components/search/searchLocation';
import CustomAlert from '../../components/CustomAlert';

const AddAnnounceScreen = ({onClose, announceId, loadOwnerAnnounces}) => {
  const {t} = useTranslation();
  const {
    clearErrorMessage,
    CreateAnnounce,
    GetAnnounceById,
    UpdateAnnounce,
    resetOperationWellDoneMessage,
  } = useRematchDispatch((dispatch) => ({
    LoadAnnouncesByOwner: dispatch.announce.LoadAnnouncesByOwner,
    resetOperationWellDoneMessage:
      dispatch.announce.resetOperationWellDoneMessage,
    clearErrorMessage: dispatch.announce.clearErrorMessage,
    CreateAnnounce: dispatch.announce.CreateAnnounce,
    GetAnnounceById: dispatch.announce.GetAnnounceById,
    UpdateAnnounce: dispatch.announce.UpdateAnnounce,
    ClearAnnounce: dispatch.announce.ClearAnnounce,
  }));

  const currentUser = useSelector((state) => state.user.currentUser);
  const operationWellDoneMessage = useSelector(
    (state) => state.announce.operationWellDoneMessage,
  );
  const errorMessage = useSelector((state) => state.announce.errorMessage);
  const announce = useSelector((state) => state.announce.announce);

  const [files, setFiles] = useState([]);

  const [title, setTitle] = useState('');
  const [body, setBody] = useState('');
  const [price, setPrice] = useState('');
  const [districtId, setDistrictId] = useState('');
  const [showAlert, setShowAlert] = useState(false);
  const [alertMessage, setAlertMessage] = useState('');
  const [location, setLocation] = useState('');
  const [phoneNumber, setPhoneNumber] = useState('');

  useFocusEffect(
    useCallback(() => {
      const profileData =
        currentUser.displayName && JSON.parse(currentUser.displayName);
      setPhoneNumber(
        profileData && profileData.phoneNumber ? profileData.phoneNumber : '',
      );
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []),
  );

  useEffect(() => {
    if (announceId) {
      setFiles([]);
      GetAnnounceById({id: announceId});
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [announceId]);

  useEffect(() => {
    if (announce !== null && announceId) {
      setDistrictId(announce.districtId);
      setTitle(announce.title);
      setBody(announce.body);
      setPrice(`${announce.price}`);
      const profileData =
        currentUser.displayName && JSON.parse(currentUser.displayName);
      const profilePhoneNumber =
        profileData && profileData.phoneNumber ? profileData.phoneNumber : '';
      setPhoneNumber(
        announce.phoneNumber ? announce.phoneNumber : profilePhoneNumber,
      );
      let currentFiles = [];
      announce.pictures.forEach((picture) => {
        currentFiles.push({
          uri: picture,
          id: Math.floor(Math.random() * Math.floor(Date.now())),
          isFormServer: true,
        });
      });
      setFiles(currentFiles);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [announce]);

  useEffect(() => {
    if (operationWellDoneMessage !== '') {
      setShowAlert(true);
      setAlertMessage(
        `${t(operationWellDoneMessage)}.\n${t('addAnnounceScreen.warning')}`,
      );
      init();
      loadOwnerAnnounces(currentUser.uid);
      //onClose();
    } else if (errorMessage !== '') {
      setShowAlert(true);
      setAlertMessage(t('common.error'));
      if (errorMessage === 'auth/unvalidated-email') {
        setAlertMessage(t('common.fireBaseError.email-not-verified'));
      }
      clearErrorMessage();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [operationWellDoneMessage, errorMessage]);

  const init = () => {
    resetOperationWellDoneMessage();
    setDistrictId();
    setLocation();
    setTitle();
    setBody();
    setPrice();
    setFiles([]);
  };

  const onBackPress = () => {
    init();
    setFiles([]);
    onClose();
  };

  const handleOnConfirmPressed = () => {
    setShowAlert(false);
    init();
    onClose();
  };

  const onPressSubmit = () => {
    if (!title || !districtId) {
      setShowAlert(true);
      setAlertMessage(t('addAnnounceScreen.error'));
      return;
    }

    const data = {
      id: announceId ? announceId : null,
      title: title,
      body: body,
      price: parseFloat(price),
      published: true,
      pictures: announceId
        ? files.filter((file) => file.isFormServer).map((file) => file.uri)
        : [],
      ownerId: currentUser.uid,
      detail: currentUser.displayName,
      districtId: districtId,
      phoneNumber: phoneNumber,
    };
    const formData = new FormData();
    files
      .filter((file) => file.type)
      .forEach((file) => {
        formData.append('images', file);
      });
    formData.append('data', JSON.stringify(data));
    if (announceId) {
      UpdateAnnounce(formData);
    } else {
      CreateAnnounce(formData);
    }
  };

  const handleLocationChange = (loc) => {
    if (loc) {
      setDistrictId(loc.id);
      setLocation(loc.location);
    }
  };

  const rightHeader = () => {
    return <Image source={logoText} style={styles.logo} />;
  };

  const deleteItem = (item) => {
    const newFiles = files;
    newFiles.splice(item.id, 1);
    setFiles([...newFiles]);
  };

  const onSelectPhotoTapped = () => {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      noData: true,
      mediaType: 'photo',
    };

    ImagePicker.launchImageLibrary(options, (response) => {
      if (response.uri && response.fileName) {
        let source = {
          uri: response.uri,
          type: response.type,
          name: response.fileName,
          id: Date.now(),
        };
        setFiles((previousState) => [...previousState, source]);
      }
    });
  };

  const renderItem = ({item}) => (
    <View style={styles.itemBody}>
      <React.Fragment>
        <Image style={styles.imageContainer} source={item} />
        <TouchableOpacity
          style={styles.deleteBtn}
          onPress={() => {
            deleteItem({item});
          }}>
          <IonIcon name="close-circle" color={Colors.royalblue} size={30} />
        </TouchableOpacity>
      </React.Fragment>
    </View>
  );

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      style={{flex: 1}}>
      <SafeAreaView style={styles.container}>
        <HeaderBar
          screenName={
            announceId
              ? t('profileScreen.modify_announce')
              : t('profileScreen.add_announce')
          }
          rightComponent={rightHeader()}
          backPress={onBackPress}
        />

        <View style={styles.headerContainer}>
          <TouchableOpacity
            onPress={onPressSubmit}
            style={styles.headerButtonContainer}>
            <MaterialIcon name="done" size={25} color={Colors.white} />
            <Text style={styles.headerButtonText}>
              {t('addAnnounceScreen.saveButton')}
            </Text>
          </TouchableOpacity>
        </View>

        <View style={styles.inputContainer}>
          {!announceId && (
            <View style={styles.searchLocation}>
              <SearchLocation
                onChange={handleLocationChange}
                locationWithDetail={true}
                hasElevation={true}
                value={location}
              />
            </View>
          )}

          <TextInput
            style={[styles.inputStyle, {marginTop: 10}]}
            placeholder={t('addAnnounceScreen.title')}
            value={title}
            onChangeText={(val) => setTitle(val)}
          />

          <TextInput
            style={styles.inputStyle}
            placeholder={t('addAnnounceScreen.description')}
            numberOfLines={3}
            multiline={true}
            value={body}
            onChangeText={(val) => setBody(val)}
          />

          <TextInput
            style={styles.inputStyle}
            keyboardType={'numeric'} // numeric  number-pad
            placeholder={t('addAnnounceScreen.price')}
            value={price}
            onChangeText={(val) => setPrice(val)}
          />

          <TextInput
            style={styles.inputStyle}
            keyboardType={'phone-pad'}
            placeholder={t('profileScreen.phoneNumber')}
            value={phoneNumber}
            onChangeText={(val) => setPhoneNumber(val)}
          />
        </View>

        <View style={styles.viewContainer}>
          <TouchableOpacity
            onPress={onSelectPhotoTapped}
            style={[styles.imageContainer, styles.imageAddContainer]}>
            <MaterialIcon name="add" size={30} color={Colors.royalblue} />
            <Text style={styles.addButtonText}>
              {t('addAnnounceScreen.addImage')}
            </Text>
          </TouchableOpacity>
          <FlatList
            style={styles.listContainer}
            horizontal={true}
            showsHorizontalScrollIndicator={true}
            data={files.sort((file1, file2) => file2.id - file1.id)}
            renderItem={renderItem}
            keyExtractor={(item) => '' + item.id}
          />
        </View>
      </SafeAreaView>
      <CustomAlert
        message={alertMessage}
        showAlert={showAlert}
        confirmText="OK"
        onCancelPressed={() => {
          setShowAlert(false);
        }}
        onConfirmPressed={() => {
          handleOnConfirmPressed();
        }}
      />
    </KeyboardAvoidingView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.clearGray,
  },
  inputContainer: {
    paddingHorizontal: 15,
    paddingVertical: 20,
  },
  viewContainer: {
    flexDirection: 'row',
    paddingRight: 15,
  },
  listContainer: {
    flex: 2,
    backgroundColor: Colors.clearGray,
  },
  itemBody: {
    backgroundColor: Colors.clearGray,
    alignSelf: 'baseline',
  },
  imageContainer: {
    borderRadius: 10,
    width: 130,
    height: 130,
    marginLeft: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  imageAddContainer: {
    borderColor: Colors.royalblue,
    borderLeftWidth: 1,
    borderTopWidth: 1,
    borderRightWidth: 1,
    borderBottomWidth: 1,
    backgroundColor: Colors.clearGray,
  },
  addButtonText: {
    color: Colors.royalblue,
    fontFamily: FONTS.OpenSansBold,
    fontSize: 13,
    textAlign: 'center',
  },
  deleteBtn: {
    paddingLeft: 10,
    position: 'absolute',
  },
  inputStyle: {
    width: '100%',
    marginBottom: 15,
    paddingBottom: 15,
    alignSelf: 'center',
    borderColor: Colors.royalblue,
    borderBottomWidth: 1,
  },
  logo: {
    marginHorizontal: 10,
  },
  headerContainer: {
    backgroundColor: Colors.meanBlue,
    paddingHorizontal: 15,
    paddingBottom: 15,
    paddingTop: 15,
    marginTop: 1,
    height: 70,
  },
  headerButtonContainer: {
    backgroundColor: Colors.royalblue,
    width: '100%',
    borderRadius: 5,
    borderColor: Colors.white,
    borderWidth: 1,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    height: 40,
  },
  headerButtonText: {
    color: Colors.white,
    fontFamily: FONTS.OpenSansBold,
    fontSize: 13,
  },
  searchLocation: {
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
});

export default AddAnnounceScreen;
