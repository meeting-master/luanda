import React, {useCallback, useEffect, useState} from 'react';
import {
  FlatList,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {useFocusEffect} from '@react-navigation/core';
import {useSelector} from 'react-redux';
import {ListItem} from 'react-native-elements';

//hooks
import {useTranslation} from '../../hooks/useTranslation';
import useRematchDispatch from '../../hooks/useRematchDispatch';

//Services
import Colors from '../../constants/Colors';
import FONTS from '../../constants/fonts';

//components
import SearchSkeleton from '../../components/search/skeleton';
import EmptyList from '../../components/emptyList';
import ButtonWithConfirmation from '../../components/ButtonWithConfirmation';

import ROUTES from '../../navigation/Routes';
import CustomModal from '../../components/CustomModal';
import AddAnnounceScreen from './AddAnnounceScreen';
import {MaterialIcon} from '../../components/Icon';
import config from '../../constants/config';

const AnnounceHistoryScreen = ({navigation}) => {
  const {t} = useTranslation();
  const {LoadAnnouncesByOwner, DeleteAnnounce, SetHeader} = useRematchDispatch(
    (dispatch) => ({
      LoadAnnouncesByOwner: dispatch.announce.LoadAnnouncesByOwner,
      DeleteAnnounce: dispatch.announce.DeleteAnnounce,
      SetHeader: dispatch.preference.SetHeader,
    }),
  );
  const loadedData = useSelector((state) => state.announce.myAnnounces);
  const isLoading = useSelector((state) => state.search.isLoading);
  const currentUser = useSelector((state) => state.user.currentUser);

  const navigator = useSelector((state) => state.preference.navigator);

  const [showModal, setShowModal] = useState(false);
  const [announceId, setAnnounceId] = useState();

  useFocusEffect(
    useCallback(() => {
      LoadAnnouncesByOwner(currentUser.uid);
      SetHeader({
        title: t('header.my_announces'),
        image: 'event-note',
        backTo: () => navigation.navigate(ROUTES.account.settings),
      });
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []),
  );

  useEffect(() => {
    if (navigator.account === true) {
      navigation.navigate(ROUTES.account.settings);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [navigator]);

  const onPressItem = (id) => {
    setAnnounceId(id);
    setModalVisibility();
  };

  const renderSubtitle = (item) => {
    let key;
    let color;
    switch (item.status) {
      case '-1':
        key = 'status.deny';
        color = Colors.red;
        break;
      case '1':
        key = 'status.valid';
        color = Colors.royalblue;
        break;
      default:
        key = 'status.exam';
        color = Colors.red;
    }

    return (
      <View>
        <View style={styles.positionView}>
          <MaterialIcon style={styles.icon} name="location-on" size={15} />
          <Text style={styles.listSubTitle}>{`${item.location}`}</Text>
        </View>
        <Text style={[styles.listSubTitle, {color: color}]}>{t(key)}</Text>
        {item.validationComment !== '' && (
          <Text style={[styles.listSubTitle, styles.error]}>
            {item.validationComment}
          </Text>
        )}
        <Text style={[styles.listTitle, styles.price]}>{`${item.price}`}</Text>
      </View>
    );
  };

  const renderTitle = (id, title) => (
    <TouchableOpacity
      onPress={() => {
        onPressItem(id);
      }}>
      <Text style={styles.listTitle}>{title.toUpperCase()}</Text>
    </TouchableOpacity>
  );

  const handleOnPress = (item) => {
    DeleteAnnounce(`${item.id}|${item.ownerId}`);
    LoadAnnouncesByOwner(item.ownerId);
  };

  const renderRightIcon = (item) => (
    <ButtonWithConfirmation onClick={() => handleOnPress(item)} />
  );

  const renderItem = ({item}) => (
    <ListItem
      title={renderTitle(item.id, item.title, item.price)}
      titleStyle={styles.listTitle}
      subtitle={renderSubtitle(item)}
      containerStyle={styles.listItem}
      leftAvatar={{
        rounded: false,
        size: 'large',
        title: item.title.substr(0, 2),
        source: {
          uri:
            item.pictures?.length > 0
              ? item.pictures[0]
              : config.DEFAULT_IMAGE_URL,
        },
        onPress: () => {
          onPressItem(item.id);
        },
      }}
      rightIcon={renderRightIcon(item)}
    />
  );

  const renderX = () => (
    <FlatList
      style={styles.listContainer}
      data={loadedData}
      renderItem={renderItem}
      keyExtractor={(item) => item.id}
      ListEmptyComponent={<EmptyList message={t('search_empty_message')} />}
    />
  );

  const setModalVisibility = () => setShowModal(!showModal);

  const onCloseModal = () => {
    setAnnounceId(undefined);
    setModalVisibility();
  };

  const Header = () => {
    return (
      <View style={styles.headerContainer}>
        <TouchableOpacity
          onPress={setModalVisibility}
          style={styles.headerButtonContainer}>
          <MaterialIcon name="add" size={25} color={Colors.white} />
          <Text style={styles.headerButtonText}>{t('add_announce')}</Text>
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <>
      <SafeAreaView style={{flex: 1}}>
        {Header()}
        <View style={styles.container}>
          {isLoading === true ? <SearchSkeleton value={3} /> : renderX()}
        </View>
      </SafeAreaView>
      <CustomModal onCloseModal={setModalVisibility} isVisible={showModal}>
        <AddAnnounceScreen
          onClose={onCloseModal}
          announceId={announceId}
          loadOwnerAnnounces={LoadAnnouncesByOwner}
        />
      </CustomModal>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.clearGray,
  },
  sliderContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 20,
  },
  sliderText: {
    fontFamily: FONTS.OpenSansItalic,
    fontSize: 12,
    marginHorizontal: 10,
  },
  slider: {
    width: '70%',
  },
  listContainer: {
    backgroundColor: Colors.clearGray,
  },
  listItem: {
    borderRadius: 5,
    marginVertical: 5,
    paddingVertical: 5,
    marginHorizontal: 15,
    paddingHorizontal: 5,
    shadowColor: Colors.black,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  listTitle: {
    fontFamily: FONTS.OpenSans,
    color: Colors.royalblue,
    fontSize: 12,
    fontWeight: 'bold',
    alignSelf: 'flex-start',
  },
  listSubTitle: {
    fontFamily: FONTS.OpenSansItalic,
    color: Colors.darkGrey,
    fontSize: 11,
  },
  error: {
    color: Colors.errorBackground,
  },
  price: {
    marginTop: 5,
  },
  positionView: {
    flexDirection: 'row',
  },
  icon: {
    marginRight: 5,
    color: Colors.darkGrey,
  },
  badge: {
    backgroundColor: Colors.blueLight,
  },
  badgeContainer: {
    backgroundColor: Colors.white,
  },
  badgeText: {
    fontFamily: FONTS.Avenir,
    color: Colors.defaultBackground,
  },
  itemBody: {
    margin: 10,
    backgroundColor: Colors.white,
    borderRadius: 5,
    shadowColor: Colors.black,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    alignSelf: 'baseline',
    width: '95%',
  },
  fab: {
    position: 'absolute',
    margin: 16,
    right: 0,
    bottom: 0,
    backgroundColor: Colors.primary,
  },
  headerContainer: {
    backgroundColor: Colors.meanBlue,
    height: 70,
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: 15,
  },
  headerButtonContainer: {
    backgroundColor: Colors.royalblue,
    width: '100%',
    borderRadius: 5,
    borderColor: Colors.white,
    borderWidth: 1,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    height: 40,
  },
  headerButtonText: {
    color: Colors.white,
    fontFamily: FONTS.OpenSansBold,
    fontSize: 13,
  },
});

export default AnnounceHistoryScreen;
