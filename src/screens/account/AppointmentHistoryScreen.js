import React, {useCallback, useEffect, useState} from 'react';
import {
  FlatList,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {useFocusEffect} from '@react-navigation/core';
import {Button, ListItem} from 'react-native-elements';
import {useSelector} from 'react-redux';
import {isBefore} from 'date-fns';
//hooks
import useRematchDispatch from '../../hooks/useRematchDispatch';
import {useTranslation} from '../../hooks/useTranslation';
//services
import Colors from '../../constants/Colors';
import FONTS from '../../constants/fonts';
import ROUTES from '../../navigation/Routes';
//components
import SearchSkeleton from '../../components/search/skeleton';
import EmptyList from '../../components/emptyList';
import ButtonWithConfirmation from '../../components/ButtonWithConfirmation';
import {MaterialIcon} from '../../components/Icon';
import CustomAlert from '../../components/CustomAlert';
import {getLanguage} from '../../translation/lang';

export default function AppointmentHistoryScreen({navigation}) {
  const {t} = useTranslation();

  const {
    LoadHistory,
    resetOperationWellDoneMessage,
    deleteHistory,
    clearErrorMessage,
    SetHeader,
  } = useRematchDispatch((dispatch) => ({
    SetHeader: dispatch.preference.SetHeader,
    LoadHistory: dispatch.user.LoadHistory,
    resetOperationWellDoneMessage: dispatch.user.resetOperationWellDoneMessage,
    deleteHistory: dispatch.user.deleteHistory,
    clearErrorMessage: dispatch.user.clearErrorMessage,
  }));
  const historyData = useSelector((state) => state.user.historyData);
  const isLoading = useSelector((state) => state.user.isLoading);
  const currentUser = useSelector((state) => state.user.currentUser);
  const operationWellDoneMessage = useSelector(
    (state) => state.user.operationWellDoneMessage,
  );
  const errorMessage = useSelector((state) => state.user.errorMessage);
  const navigator = useSelector((state) => state.preference.navigator);

  const [showAlert, setShowAlert] = useState(false);
  const [alertMessage, setAlertMessage] = useState('');
  const [nextAppointments, setNextAppointments] = useState([]);
  const [passedAppointments, setPassedAppointments] = useState([]);

  useFocusEffect(
    useCallback(() => {
      if (currentUser !== null) {
        LoadHistory(currentUser.uid);
      }
      SetHeader({
        title: t('header.my_appointments'),
        image: 'event-note',
        backTo: () => navigation.navigate(ROUTES.account.settings),
      });
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [currentUser]),
  );

  useEffect(() => {
    if (navigator.account === true) {
      navigation.navigate(ROUTES.account.settings);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [navigator]);

  // willMount
  useEffect(() => {
    return () => {
      resetOperationWellDoneMessage();
      clearErrorMessage();
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (operationWellDoneMessage !== '') {
      setShowAlert(true);
      setAlertMessage(t(operationWellDoneMessage));
      LoadHistory(currentUser.uid);
      resetOperationWellDoneMessage();
      clearErrorMessage();
    } else if (errorMessage !== '') {
      setShowAlert(true);
      setAlertMessage(t('common.error'));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [operationWellDoneMessage, errorMessage]);

  useEffect(() => {
    let next = [];
    let passed = [];
    historyData.forEach((item) => {
      isBefore(Date.parse(item.startDate), Date.now())
        ? passed.push(item)
        : next.push(item);
    });
    setNextAppointments(next);
    setPassedAppointments(passed);
  }, [historyData]);

  const format = (date) => {
    if (getLanguage() === 'fr') {
      const split = date.split('-');
      return split[2] + '-' + split[1] + '-' + split[0];
    } else {
      return date;
    }
  };

  const renderSubtitle = (item) => (
    <View style={styles.listTitleContainer}>
      <View style={styles.positionView}>
        <MaterialIcon style={styles.icon} name="location-on" size={15} />
        <Text
          style={
            styles.listSubTitle
          }>{`${item.streetNumber} ${item.streetName}`}</Text>
      </View>
      <View style={styles.positionView}>
        <MaterialIcon style={styles.icon} name="flag" size={15} />
        <Text style={styles.listSubTitle}>{`${item.serviceName}`}</Text>
      </View>
    </View>
  );

  const renderItem = ({item}) => (
    <ListItem
      key={item.id}
      title={item.customerName}
      titleStyle={styles.listTitle}
      subtitle={renderSubtitle(item)}
      subtitleStyle={styles.listSubTitle}
      containerStyle={styles.listItem}
      rightTitle={format(item.startDate)}
      rightTitleStyle={styles.rightListTitle}
      rightSubtitle={item.startTime}
      rightSubtitleStyle={styles.rightListSubTitle}
      rightIcon={renderButtonWithConfirmation(item)}
    />
  );

  const renderButtonWithConfirmation = (item) =>
    new Date(item.startDate) <= new Date() ? (
      <Button
        onPress={() => {
          navigation.navigate(ROUTES.account.rating, {
            appointmentId: item.appointmentId,
          });
        }}
        type={'clear'}
        icon={
          <MaterialIcon
            name="thumbs-up-down"
            size={25}
            color={Colors.meanBlue}
          />
        }
      />
    ) : (
      <ButtonWithConfirmation
        onClick={() => handleOnPress(item)}
        disabled={new Date(item.date) <= new Date()}
      />
    );

  const handleOnPress = (item) => {
    deleteHistory({
      data: `${item.appointmentId}|${item.owner}`,
    });
  };

  const renderX = () => (
    <FlatList
      style={styles.listContainer}
      data={nextAppointments}
      renderItem={renderItem}
      keyExtractor={(item) => item.appointmentId}
      ListEmptyComponent={<EmptyList message={t('no_history')} />}
    />
  );
  const renderP = () => (
    <FlatList
      style={styles.listContainer}
      data={passedAppointments}
      renderItem={renderItem}
      keyExtractor={(item) => item.appointmentId}
      ListEmptyComponent={<EmptyList message={t('no_history')} />}
    />
  );
  const Header = (title, icon) => {
    return (
      <View style={styles.headerContainer}>
        <TouchableOpacity style={styles.headerButtonContainer}>
          <MaterialIcon name={icon} size={25} color={Colors.white} />
          <Text style={styles.headerButtonText}>{title}</Text>
        </TouchableOpacity>
      </View>
    );
  };
  return (
    <SafeAreaView style={styles.container}>
      {nextAppointments.length > 0 && Header(t('to_come'), 'queue-play-next')}
      {isLoading === true ? (
        <SearchSkeleton value={3} />
      ) : (
        nextAppointments.length > 0 && renderX()
      )}
      {Header(t('history'), 'history')}
      {renderP()}
      <CustomAlert
        message={alertMessage}
        showAlert={showAlert}
        confirmText="OK"
        onCancelPressed={() => {
          setShowAlert(false);
        }}
        onConfirmPressed={() => {
          setShowAlert(false);
        }}
      />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.defaultBackground,
  },
  listContainer: {
    backgroundColor: Colors.greyhish,
    marginHorizontal: 10,
    minHeight: 200,
  },
  listItem: {
    borderRadius: 10,
    marginVertical: 5,
  },
  listTitle: {
    fontFamily: FONTS.OpenSans,
    color: Colors.meanBlue,
    fontSize: 12,
  },
  listSubTitle: {
    fontFamily: FONTS.OpenSans,
    color: Colors.darkGrayOne,
    fontSize: 11,
  },
  rightListTitle: {
    fontFamily: FONTS.OpenSans,
    fontSize: 12,
    color: Colors.royalblue,
  },
  rightListSubTitle: {
    fontFamily: FONTS.OpenSans,
    fontSize: 11,
  },
  headerContainer: {
    backgroundColor: Colors.meanBlue,
    height: 70,
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: 15,
  },
  headerButtonContainer: {
    backgroundColor: Colors.royalblue,
    width: '100%',
    borderRadius: 5,
    borderColor: Colors.white,
    borderWidth: 1,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    height: 40,
  },
  headerButtonText: {
    color: Colors.white,
    fontFamily: FONTS.OpenSansBold,
    fontSize: 13,
    marginLeft: 5,
  },
  positionView: {
    flexDirection: 'row',
  },
  icon: {
    marginRight: 5,
    color: Colors.meanBlue,
  },
});
