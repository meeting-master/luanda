import React, {useEffect, useState, useCallback} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  Alert,
  ActivityIndicator,
  KeyboardAvoidingView,
  Platform,
} from 'react-native';
import {useFocusEffect} from '@react-navigation/core';
import {useSelector} from 'react-redux';

//hooks
import useRematchDispatch from '../../hooks/useRematchDispatch';
import {useTranslation} from '../../hooks/useTranslation';
import {useForm, Controller} from 'react-hook-form';

//components
import SearchSkeleton from '../../components/search/skeleton';
import Colors from '../../constants/Colors';
import ROUTES from '../../navigation/Routes';
import CustomAlert from '../../components/CustomAlert';

const PasswordScreen = ({navigation}) => {
  const {t} = useTranslation();

  const {
    updatePassword,
    clearErrorMessage,
    clearRegisterUser,
    SetHeader,
  } = useRematchDispatch((dispatch) => ({
    SetHeader: dispatch.preference.SetHeader,
    updatePassword: dispatch.user.updatePassword,
    clearErrorMessage: dispatch.user.clearErrorMessage,
    clearRegisterUser: dispatch.user.clearRegisterUser,
  }));
  const registerUser = useSelector((state) => state.user.registerUser);
  const isLoading = useSelector((state) => state.user.isLoading);
  const errorMessage = useSelector((state) => state.user.errorMessage);

  const {control, handleSubmit, errors, formState, getValues} = useForm({
    mode: 'onChange',
  });

  const [showAlert, setShowAlert] = useState(false);
  const [alertMessage, setAlertMessage] = useState('');

  useFocusEffect(
    useCallback(() => {
      SetHeader({
        title: t('header.password'),
        image: 'lock-outline',
        backTo: () => navigation.navigate(ROUTES.root),
      });
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []),
  );

  useEffect(() => {
    return () => {
      clearErrorMessage();
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (registerUser !== null) {
      clearRegisterUser();
      setShowAlert(true)
      setAlertMessage(t('updatePasswordScreen.successful_update'))
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [registerUser]);

  useEffect(() => {
    if (errorMessage !== '') {
      setShowAlert(true)
      switch (errorMessage) {
        case 'auth/wrong-password':
          setAlertMessage(t('common.fireBaseError.wrong-password'))
          break;
        default:
          setAlertMessage(t('common.error'))
          console.log(errorMessage);
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [errorMessage]);

  const handleOnPress = (data) => {
    updatePassword(data);
  };

  const validatePwdConfirmation = () => {
    if (getValues('confirmPassword') !== getValues('password')) {
      return false;
    }
    return true;
  };

  const handleOnConfirmPressed = () => {
    if (registerUser !== null) {
      navigation.navigate(ROUTES.root)
    }
    setShowAlert(false)
  }

  if (isLoading) {
    return <SearchSkeleton value={3} />;
  }
  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      style={{flex: 1}}>
      <View style={styles.container}>
        {errors.oldPassword && (
          <Text style={styles.errorText}>{t('common.errors.oldPassword')}</Text>
        )}
        <Controller
          control={control}
          render={({onChange, onBlur, value}) => (
            <TextInput
              style={styles.inputStyle}
              placeholder={t('updatePasswordScreen.oldPassword')}
              maxLength={15}
              secureTextEntry={true}
              onBlur={onBlur}
              onChangeText={(value) => onChange(value)}
              value={value}
            />
          )}
          name="oldPassword"
          rules={{required: true}}
          defaultValue=""
        />

        {errors.password && (
          <Text style={styles.errorText}>{t('common.errors.password')}</Text>
        )}
        <Controller
          control={control}
          render={({onChange, onBlur, value}) => (
            <TextInput
              style={styles.inputStyle}
              placeholder={t('updatePasswordScreen.password')}
              maxLength={15}
              secureTextEntry={true}
              onBlur={onBlur}
              onChangeText={(value) => onChange(value)}
              value={value}
            />
          )}
          name="password"
          rules={{required: true}}
          defaultValue=""
        />

        {errors.confirmPassword && (
          <Text style={styles.errorText}>
            {t('common.errors.confirmPasswordError')}
          </Text>
        )}
        <Controller
          control={control}
          render={({onChange, onBlur, value}) => (
            <TextInput
              style={styles.inputStyle}
              placeholder={t('updatePasswordScreen.confirmation')}
              maxLength={15}
              secureTextEntry={true}
              onBlur={onBlur}
              onChangeText={(value) => onChange(value)}
              value={value}
            />
          )}
          name="confirmPassword"
          rules={{required: true, validate: validatePwdConfirmation}}
          defaultValue=""
        />

        <Button
          color={Colors.primary}
          title={t('updatePasswordScreen.buttonLabel')}
          disabled={!formState.isValid}
          onPress={handleSubmit(handleOnPress)}
        />
        <CustomAlert
          message={alertMessage}
          showAlert={showAlert}
          confirmText='OK'
          onCancelPressed = {() => {setShowAlert(false)}}
          onConfirmPressed = {() => {handleOnConfirmPressed()}}
        />
      </View>
    </KeyboardAvoidingView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    padding: 35,
    backgroundColor: '#fff',
  },
  inputStyle: {
    width: '100%',
    marginBottom: 15,
    paddingBottom: 15,
    alignSelf: 'center',
    borderColor: Colors.primary,
    borderBottomWidth: 1,
  },
  loginText: {
    color: Colors.primary,
    marginTop: 25,
    textAlign: 'center',
  },
  errorText: {
    color: Colors.red,
  },
  preloader: {
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff',
  },
});

export default PasswordScreen;
