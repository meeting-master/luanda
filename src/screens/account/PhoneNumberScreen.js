import React from 'react';
import {StyleSheet, SafeAreaView} from 'react-native';

import {useTranslation} from '../../hooks/useTranslation';

const PhoneNumberScreen = () => {
  const {t} = useTranslation();

  return <SafeAreaView style={styles.container} />;
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  image: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },
  logo: {
    fontSize: 20,
    fontWeight: 'bold',
  },
});

export default PhoneNumberScreen;
