import React, {useCallback, useEffect, useState} from 'react';
import {
  StyleSheet,
  View,
  TextInput,
  Button,
  Alert,
  Picker,
  KeyboardAvoidingView,
  Platform,
  Text,
} from 'react-native';
import {useSelector} from 'react-redux';
// import Picker from '@react-native-community/picker'

//hooks
import useRematchDispatch from '../../hooks/useRematchDispatch';
import {useTranslation} from '../../hooks/useTranslation';
import {useForm, Controller} from 'react-hook-form';

//components
import SearchSkeleton from '../../components/search/skeleton';
import Colors from '../../constants/Colors';
import ROUTES from '../../navigation/Routes';
import {useFocusEffect} from '@react-navigation/core';
import CustomAlert from '../../components/CustomAlert';

const ProfileScreen = ({navigation, props}) => {
  const {t} = useTranslation();

  const {UpdateAccount, clearErrorMessage, SetHeader} = useRematchDispatch(
    (dispatch) => ({
      UpdateAccount: dispatch.user.UpdateAccount,
      clearErrorMessage: dispatch.user.clearErrorMessage,
      SetHeader: dispatch.preference.SetHeader,
    }),
  );
  const currentUser = useSelector((state) => state.user.currentUser);
  const isLoading = useSelector((state) => state.user.isLoading);
  const errorMessage = useSelector((state) => state.user.errorMessage);
  const backTo = useSelector((state) => state.preference.backTo);
  const profileData = currentUser.displayName && JSON.parse(currentUser.displayName);
  const profileFormData = {
    firstName:
      profileData && profileData.firstName ? profileData.firstName : '',
    lastName: profileData && profileData.lastName ? profileData.lastName : '',
    phoneNumber:
      profileData && profileData.phoneNumber ? profileData.phoneNumber : '',
    gender: profileData && profileData.gender ? profileData.gender : '',
  };
  const {control, handleSubmit, errors, formState} = useForm({
    mode: 'onChange',
    defaultValues: profileFormData,
  });
  const [showAlert, setShowAlert] = useState(false);

  useFocusEffect(
    useCallback(() => {
      SetHeader({
        title: t('header.profile'),
        image: 'person-outline',
        backTo: () => navigation.navigate(ROUTES.root),
      });
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []),
  );

  // willMount
  useEffect(() => {
    return () => {
      clearErrorMessage();
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (errorMessage !== '') {
      setShowAlert(true)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [errorMessage]);

  const handleOnPress = (data) => {
    UpdateAccount(data);
    navigation.navigate(backTo ? backTo : ROUTES.root);
  };

  if (isLoading) {
    return <SearchSkeleton value={3} />;
  }
  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      style={{flex: 1}}>
      <View style={styles.container}>
        {errors.firstName && (
          <Text style={styles.errorText}>{t('common.errors.firstName')}</Text>
        )}
        <Controller
          control={control}
          render={({onChange, onBlur, value}) => (
            <TextInput
              style={styles.inputStyle}
              placeholder={t('profileScreen.firstName')}
              onBlur={onBlur}
              onChangeText={(value) => onChange(value)}
              value={value}
            />
          )}
          name="firstName"
          rules={{required: true}}
          defaultValue=""
        />

        {errors.lastName && (
          <Text style={styles.errorText}>{t('common.errors.lastName')}</Text>
        )}
        <Controller
          control={control}
          render={({onChange, onBlur, value}) => (
            <TextInput
              style={styles.inputStyle}
              placeholder={t('profileScreen.lastName')}
              onBlur={onBlur}
              onChangeText={(value) => onChange(value)}
              value={value}
            />
          )}
          name="lastName"
          rules={{required: true}}
          defaultValue=""
        />

        <Controller
          control={control}
          render={({onChange, onBlur, value}) => (
            <Picker
              style={styles.inputStyle}
              selectedValue={value}
              onValueChange={(value) => onChange(value)}>
              <Picker.Item label={t('profileScreen.gender')} value="" />
              <Picker.Item label={t('profileScreen.women')} value="F" />
              <Picker.Item label={t('profileScreen.man')} value="M" />
            </Picker>
          )}
          name="gender"
          defaultValue=""
        />

        {errors.phoneNumber && (
          <Text style={styles.errorText}>{t('common.errors.phoneNumber')}</Text>
        )}
        <Controller
          control={control}
          render={({onChange, onBlur, value}) => (
            <TextInput
              style={styles.inputStyle}
              keyboardType={'phone-pad'}
              placeholder={t('profileScreen.phoneNumber')}
              onBlur={onBlur}
              onChangeText={(value) => onChange(value)}
              value={value}
            />
          )}
          name="phoneNumber"
          rules={{
            required: false,
            // pattern: /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/g,
          }}
          defaultValue=""
        />

        <Button
          color={Colors.primary}
          title={t('profileScreen.buttonLabel')}
          disabled={!formState.isValid}
          onPress={handleSubmit(handleOnPress)}
        />
        <CustomAlert
          message={t('common.error')}
          showAlert={showAlert}
          confirmText="OK"
          onCancelPressed={() => {
            setShowAlert(false);
          }}
          onConfirmPressed={() => {
            setShowAlert(false);
          }}
        />
      </View>
    </KeyboardAvoidingView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    padding: 35,
    backgroundColor: '#fff',
  },
  inputStyle: {
    width: '100%',
    marginBottom: 15,
    paddingBottom: 15,
    alignSelf: 'center',
    borderColor: Colors.primary,
    borderBottomWidth: 1,
  },
  loginText: {
    color: Colors.primary,
    marginTop: 25,
    textAlign: 'center',
  },
  errorText: {
    color: Colors.red,
  },
  preloader: {
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff',
  },
});

export default ProfileScreen;
