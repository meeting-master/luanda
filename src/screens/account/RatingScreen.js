import React, {useCallback, useEffect, useState} from 'react';
import {StyleSheet, View, TextInput, Button, Alert} from 'react-native';
import {useFocusEffect} from '@react-navigation/core';
import {useSelector} from 'react-redux';
import {AirbnbRating} from 'react-native-ratings';

//hooks
import useRematchDispatch from '../../hooks/useRematchDispatch';
import {useTranslation} from '../../hooks/useTranslation';

//components
import SearchSkeleton from '../../components/search/skeleton';
import Colors from '../../constants/Colors';
import ROUTES from '../../navigation/Routes';
import CustomAlert from '../../components/CustomAlert';

const RatingScreen = (props) => {
  const appointmentId = props.route.params.appointmentId;
  const {t} = useTranslation();
  const [rating, setRating] = useState(0);
  const [comment, setComment] = useState('');
  const [showAlert, setShowAlert] = useState(false);

  const {
    rateAndComment,
    clearErrorMessage,
    resetOperationWellDoneMessage,
    SetHeader,
  } = useRematchDispatch((dispatch) => ({
    rateAndComment: dispatch.user.rateAndComment,
    clearErrorMessage: dispatch.user.clearErrorMessage,
    resetOperationWellDoneMessage: dispatch.user.resetOperationWellDoneMessage,
    SetHeader: dispatch.preference.SetHeader,
  }));
  const isLoading = useSelector((state) => state.user.isLoading);
  const errorMessage = useSelector((state) => state.user.errorMessage);
  const operationWellDoneMessage = useSelector(
    (state) => state.user.operationWellDoneMessage,
  );

  useFocusEffect(
    useCallback(() => {
      SetHeader({
        title: t('header.rating'),
        image: 'timer',
        backTo: () =>
          props.navigation.navigate(ROUTES.account.appointmentHistory),
      });
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []),
  );

  useEffect(() => {
    if (operationWellDoneMessage !== '') {
      props.navigation.navigate('history');
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [operationWellDoneMessage]);
  // willMount
  useEffect(() => {
    return () => {
      resetOperationWellDoneMessage();
      clearErrorMessage();
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleOnPress = () => {
    if (rating === 0) {
      setShowAlert(true)
    } else {
      rateAndComment({
        rating: rating,
        comment: comment,
        appointmentId: appointmentId,
      });
    }
  };
  const ratingColor = (r) => {
    switch (r) {
      case 1:
        return Colors.rating.execrable;
      case 2:
        return Colors.rating.bad;
      case 3:
        return Colors.rating.medium;
      case 4:
        return Colors.rating.prettyGood;
      case 5:
        return Colors.rating.good;
      default:
        return Colors.dark;
    }
  };
  const ratingReviews = [
    t('ratingScreen.ratingReviews.execrable'),
    t('ratingScreen.ratingReviews.bad'),
    t('ratingScreen.ratingReviews.medium'),
    t('ratingScreen.ratingReviews.prettyGood'),
    t('ratingScreen.ratingReviews.good'),
  ];

  if (isLoading) {
    return <SearchSkeleton value={3} />;
  }
  return (
    <View style={styles.container}>
      <AirbnbRating
        showRating={true}
        selectedColor={ratingColor(rating)}
        reviewColor={ratingColor(rating)}
        reviews={ratingReviews}
        defaultRating={rating}
        size={25}
        count={5}
        reviewSize={20}
        onFinishRating={(value) => setRating(value)}
      />
      <TextInput
        style={styles.inputStyle}
        placeholder={t('ratingScreen.comment')}
        numberOfLines={5}
        multiline={true}
        value={comment}
        onChangeText={(val) => setComment(val)}
      />
      <Button
        color={Colors.primary}
        title={t('ratingScreen.buttonLabel')}
        onPress={() => handleOnPress()}
      />
      <CustomAlert
        message={t('common.missingInformation')}
        showAlert={showAlert}
        confirmText='OK'
        onCancelPressed = {() => {setShowAlert(false)}}
        onConfirmPressed = {() => {setShowAlert(false)}}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    padding: 35,
    backgroundColor: Colors.clearGray,
  },
  inputStyle: {
    width: '100%',
    marginBottom: 15,
    paddingBottom: 15,
    alignSelf: 'center',
    borderColor: Colors.primary,
    borderBottomWidth: 1,
  },
});

export default RatingScreen;
