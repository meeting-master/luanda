import React, {useCallback} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';

import Colors from '../../constants/Colors';
import {ListItem} from 'react-native-elements';
import {IonIcon, MaterialIcon} from '../../components/Icon';
import {useTranslation} from '../../hooks/useTranslation';
import {useFocusEffect} from '@react-navigation/core';
import useRematchDispatch from '../../hooks/useRematchDispatch';
import {useSelector} from 'react-redux';
import FONTS from '../../constants/fonts';
import ROUTES from '../../navigation/Routes';

const SettingsScreen = ({navigation, route}) => {
  const {t} = useTranslation();
  const {SetHeader, logout, SetNavigator} = useRematchDispatch((dispatch) => ({
    SetHeader: dispatch.preference.SetHeader,
    SetNavigator: dispatch.preference.SetNavigator,
    logout: dispatch.user.logout,
  }));

  const currentUser = useSelector((state) => state.user.currentUser);

  useFocusEffect(
    useCallback(() => {
      SetNavigator({announce: false, appointment: false, account: false});
      SetHeader({title: t('label_account'), image: 'person-outline'});
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []),
  );

  const renderItem = (
    destination,
    name,
    icon,
    type,
    disabled = false,
    avatarSize = 22,
    fittedPadding = 3,
  ) =>
    !disabled && (
      <TouchableOpacity
        style={{paddingLeft: fittedPadding}}
        disabled={disabled}
        onPress={() => {
          if (destination === ROUTES.root) {
            logout();
          }
          navigation.navigate(destination);
        }}>
        <ListItem
          title={name}
          titleStyle={disabled ? styles.disabledListTitle : styles.listTitle}
          bottomDivider
          chevron
          leftAvatar={
            type !== null ? (
              <IonIcon
                name={icon}
                size={avatarSize}
                color={disabled ? Colors.grey : Colors.darkGrayTwo}
              />
            ) : (
              <MaterialIcon
                name={icon}
                size={avatarSize}
                color={disabled ? Colors.grey : Colors.darkGrayTwo}
              />
            )
          }
        />
      </TouchableOpacity>
    );

  const renderGroup = (name, hidden = false) =>
    !hidden && (
      <ListItem
        title={name}
        bottomDivider
        containerStyle={styles.groupContainer}
        titleStyle={styles.groupText}
      />
    );

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView>
        {renderGroup(
          currentUser !== null && currentUser.displayName && JSON.parse(currentUser.displayName).firstName
            ? JSON.parse(currentUser.displayName).firstName
            : t('setting_profile'),
        )}
        {renderItem(
          ROUTES.login,
          t('setting_login'),
          'log-in-outline',
          'ion',
          currentUser !== null,
        )}
        {renderItem(
          ROUTES.password,
          t('setting_change_password'),
          'lock-outline',
          null,
          currentUser === null,
        )}
        {renderItem(
          ROUTES.email,
          t('setting_email'),
          'mail-outline',
          'ion',
          currentUser === null,
          20,
          5,
        )}
        {renderItem(
          ROUTES.profile,
          t('profileScreen.title'),
          'person-outline',
          null,
          currentUser === null,
          26,
          0,
        )}
        {renderGroup(t('setting_services'), currentUser === null)}
        {renderItem(
          ROUTES.account.announceHistory,
          t('profileScreen.announces'),
          'event-note',
          null,
          currentUser === null,
        )}
        {renderItem(
          ROUTES.account.appointmentHistory,
          t('profileScreen.appointments'),
          'timer',
          null,
          currentUser === null,
        )}
        {renderGroup(t('setting_help_title'))}
        {renderItem(
          ROUTES.about,
          t('setting_about'),
          'alert-circle-outline',
          'ion',
        )}
        {renderItem(
          ROUTES.help,
          t('setting_help'),
          'help-circle-outline',
          'ion',
        )}
        {renderItem(
          ROUTES.root,
          t('setting_log_out'),
          'log-out-outline',
          'ion',
          currentUser === null,
        )}
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.defaultBackground,
  },
  groupContainer: {
    backgroundColor: Colors.meanBlue,
    color: Colors.white,
    fontSize: 13,
  },
  groupText: {
    color: Colors.white,
    fontSize: 14,
    fontFamily: FONTS.OpenSansBold,
  },
  listTitle: {
    fontFamily: FONTS.OpenSans,
    color: Colors.dark,
    fontSize: 12,
  },
  disabledListTitle: {
    fontFamily: FONTS.OpenSans,
    color: Colors.grey,
    fontSize: 12,
  },
  enabledListTitle: {
    color: Colors.dark,
  },
});
export default SettingsScreen;
