import React, {useEffect, useState, useCallback} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  Alert,
} from 'react-native';
import {useFocusEffect} from '@react-navigation/core';
import {useSelector} from 'react-redux';

//hooks
import useRematchDispatch from '../../hooks/useRematchDispatch';
import {useTranslation} from '../../hooks/useTranslation';
import {useForm, Controller} from 'react-hook-form';

//components
import SearchSkeleton from '../../components/search/skeleton';
import Colors from '../../constants/Colors';
import ROUTES from '../../navigation/Routes';
import CustomAlert from '../../components/CustomAlert';

const UpdateEmailScreen = ({navigation}) => {
  const {t} = useTranslation();

  const {
    SetHeader,
    updateEmail,
    clearErrorMessage,
    clearRegisterUser,
  } = useRematchDispatch((dispatch) => ({
    SetHeader: dispatch.preference.SetHeader,
    updateEmail: dispatch.user.updateEmail,
    clearErrorMessage: dispatch.user.clearErrorMessage,
    clearRegisterUser: dispatch.user.clearRegisterUser,
  }));
  const currentUser = useSelector((state) => state.user.currentUser);
  const registerUser = useSelector((state) => state.user.registerUser);
  const isLoading = useSelector((state) => state.user.isLoading);
  const errorMessage = useSelector((state) => state.user.errorMessage);

  const profileFormData = {
    email: currentUser && currentUser.email ? currentUser.email : '',
  };
  const {control, handleSubmit, errors, formState} = useForm({
    mode: 'onChange',
    defaultValues: profileFormData,
  });

  const [showAlert, setShowAlert] = useState(false);
  const [alertMessage, setAlertMessage] = useState('');

  useFocusEffect(
    useCallback(() => {
      SetHeader({
        title: t('header.email'),
        image: 'person-outline',
        backTo: () => navigation.navigate(ROUTES.root),
      });
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []),
  );

  useEffect(() => {
    return () => {
      clearErrorMessage();
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (registerUser !== null) {
      clearRegisterUser();
      setShowAlert(true)
      setAlertMessage(t('updateEmailScreen.successful_update'))
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [registerUser]);

  useEffect(() => {
    if (errorMessage !== '') {
      setShowAlert(true)
      switch (errorMessage) {
        case 'auth/email-already-in-use':
          setAlertMessage(t('common.fireBaseError.email-already-in-use'))
          break;
        case 'auth/invalid-email':
          setAlertMessage(t('common.fireBaseError.invalid-email'))
          break;
        case 'auth/wrong-password':
          setAlertMessage(t('common.fireBaseError.wrong-password'))
          break;
        default:
          console.log(errorMessage);
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [errorMessage]);

  const handleOnPress = (data) => {
    updateEmail(data);
  };

  const handleOnConfirmPressed = () => {
    if (registerUser !== null) {
      navigation.navigate(ROUTES.root)
    }
    setShowAlert(false)
  }

  if (isLoading) {
    return <SearchSkeleton value={3} />;
  }
  return (
    <View style={styles.container}>
      {errors.email && (
        <Text style={styles.errorText}>{t('common.errors.email')}</Text>
      )}
      <Controller
        control={control}
        render={({onChange, onBlur, value}) => (
          <TextInput
            keyboardType={'email-address'}
            style={styles.inputStyle}
            placeholder={t('email')}
            onBlur={onBlur}
            onChangeText={(value) => onChange(value)}
            value={value}
          />
        )}
        name="email"
        rules={{required: true, pattern: /^[\w-\.\+]+@([\w-]+\.)+[\w-]{2,4}$/g}}
        defaultValue=""
      />
      {errors.password && (
        <Text style={styles.errorText}>{t('common.errors.password')}</Text>
      )}
      <Controller
        control={control}
        render={({onChange, onBlur, value}) => (
          <TextInput
            style={styles.inputStyle}
            placeholder={t('password')}
            maxLength={15}
            secureTextEntry={true}
            onBlur={onBlur}
            onChangeText={(value) => onChange(value)}
            value={value}
          />
        )}
        name="password"
        rules={{required: true}}
        defaultValue=""
      />
      <Button
        color={Colors.primary}
        title={t('updateEmailScreen.buttonLabel')}
        disabled={!formState.isValid}
        onPress={handleSubmit(handleOnPress)}
      />
      <CustomAlert
        message={alertMessage}
        showAlert={showAlert}
        confirmText='OK'
        onCancelPressed = {() => {setShowAlert(false)}}
        onConfirmPressed = {() => {handleOnConfirmPressed()}}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    padding: 35,
    backgroundColor: '#fff',
  },
  inputStyle: {
    width: '100%',
    marginBottom: 15,
    paddingBottom: 15,
    alignSelf: 'center',
    borderColor: Colors.primary,
    borderBottomWidth: 1,
  },
  loginText: {
    color: Colors.primary,
    marginTop: 25,
    textAlign: 'center',
  },
  errorText: {
    color: Colors.red,
  },
  preloader: {
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff',
  },
});

export default UpdateEmailScreen;
