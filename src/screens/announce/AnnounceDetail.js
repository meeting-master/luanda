import React, {useCallback, useState} from 'react';
import {
  StyleSheet,
  SafeAreaView,
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Dimensions,
} from 'react-native';
import {useFocusEffect} from '@react-navigation/core';
import {useSelector} from 'react-redux';
import {IonIcon, MaterialIcon} from '../../components/Icon';

//hooks
import {useTranslation} from '../../hooks/useTranslation';
import useRematchDispatch from '../../hooks/useRematchDispatch';

//Services
import Colors from '../../constants/Colors';
import FONTS from '../../constants/fonts';

//components
import SearchSkeleton from '../../components/search/skeleton';
import {SliderBox} from 'react-native-image-slider-box';
import Placeholder from '../../assets/images/Placeholder.png';

const AnnounceDetail = ({onClose, announceId}) => {
  const {t} = useTranslation();
  const {GetAnnounceById} = useRematchDispatch((dispatch) => ({
    GetAnnounceById: dispatch.announce.GetAnnounceById,
  }));
  const announce = useSelector((state) => state.announce.announce);
  const isLoading = useSelector((state) => state.search.isLoading);

  const [width, setWidth] = useState();

  useFocusEffect(
    useCallback(() => {
      GetAnnounceById({id: announceId});
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []),
  );

  const onLayout = () => {
    setWidth(Math.round(Dimensions.get('window').width));
  };

  const formatImagesFromPictures = (pictures) => {
    return pictures?.length > 0 && pictures[0] !== 'pic1'
      ? pictures
      : [Placeholder];
  };

  const timeSinceAnnounceCreation = (createdAt) => {
    const diffTime = Math.abs(new Date() - new Date(createdAt));
    const diffDays = Math.trunc(diffTime / (1000 * 60 * 60 * 24));
    if (diffDays === 0) {
      const diffHours = Math.trunc(diffTime / (1000 * 60 * 60));
      if (diffHours === 0) {
        const diffMinutes = Math.trunc(diffTime / (1000 * 60));
        return `${t('announceScreen.minute', {count: diffMinutes})}`;
      }
      return `${t('announceScreen.hour', {count: diffHours})}`;
    }
    return t('announceScreen.day', {count: diffDays});
  };

  const renderAnnounce = () => (
    <>
      <ScrollView onLayout={onLayout}>
        <SliderBox
          images={formatImagesFromPictures(announce?.pictures)}
          dotColor={Colors.royalblue}
          imageLoadingColor={Colors.royalblue}
          autoplay={false}
          resizeMethod={'auto'}
          resizeMode={'cover'}
          sliderBoxHeight={300}
          parentWidth={width}
          ImageComponentStyle={styles.imageComponent}
        />
        <View style={styles.header}>
          <TouchableOpacity onPress={onClose}>
            <IonIcon
              name="close-circle"
              color={Colors.royalblue}
              size={30}
              style={styles.iconStyle}
            />
          </TouchableOpacity>
        </View>
        <Text
          style={styles.listTitle}>{`${announce?.title.toUpperCase()}`}</Text>
        <View style={styles.positionView}>
          <MaterialIcon style={styles.icon} name="location-on" size={15} />
          <Text style={styles.listSubTitle}>{`${
            announce?.location
          } ${timeSinceAnnounceCreation(announce?.createdAt)}`}</Text>
        </View>
        <Text style={styles.details}>{`${announce?.body}`}</Text>
        {announce?.phoneNumber ? (
          <View style={styles.positionView}>
            <MaterialIcon style={styles.icon} name="phone" size={15} />
            <Text style={styles.listSubTitle}>{`${
              announce?.phoneNumber ? announce?.phoneNumber : ''
            }`}</Text>
          </View>
        ) : (
          <></>
        )}
        <Text style={styles.price}>{`${announce?.price}`}</Text>
      </ScrollView>
    </>
  );

  return (
    <SafeAreaView style={styles.container}>
      {isLoading === true ? <SearchSkeleton value={5} /> : renderAnnounce()}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.defaultBackground,
  },
  slider: {
    width: '70%',
  },
  listTitle: {
    fontFamily: FONTS.OpenSans,
    color: Colors.royalblue,
    fontSize: 20,
    fontWeight: 'bold',
    alignSelf: 'flex-start',
    padding: 15,
  },
  listSubTitle: {
    fontFamily: FONTS.Avenir,
    color: Colors.darkGrey,
    fontSize: 11,
  },
  price: {
    fontFamily: FONTS.OpenSans,
    color: Colors.royalblue,
    fontSize: 20,
    fontWeight: 'bold',
    alignSelf: 'flex-start',
    paddingLeft: 15,
    marginTop: 10,
  },
  details: {
    fontFamily: FONTS.Avenir,
    color: Colors.darkDarkGrey,
    fontSize: 13,
    padding: 15,
  },
  positionView: {
    flexDirection: 'row',
    paddingLeft: 15,
    paddingRight: 15,
  },
  icon: {
    color: Colors.darkGrey,
  },
  header: {
    padding: 10,
    position: 'absolute',
  },
  imageComponent: {
    flex: 1,
  },
  iconStyle: {
    shadowOpacity: 0.75,
    shadowRadius: 3.84,
    elevation: 5,
  },
});

export default AnnounceDetail;
