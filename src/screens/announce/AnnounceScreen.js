import React, {useCallback, useEffect, useState} from 'react';
import {
  FlatList,
  Keyboard,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {useFocusEffect} from '@react-navigation/core';
import {useSelector} from 'react-redux';
import {ListItem} from 'react-native-elements';

//hooks
import {useTranslation} from '../../hooks/useTranslation';
import useRematchDispatch from '../../hooks/useRematchDispatch';

//Services
import Colors from '../../constants/Colors';
import FONTS from '../../constants/fonts';

//components
import SearchSkeleton from '../../components/search/skeleton';
import EmptyList from '../../components/emptyList';
import AnnounceDetail from './AnnounceDetail';
import EasySlider from '../../components/Slider';
import EasySearchBar from '../../components/search/searchBar';
import {MaterialIcon} from '../../components/Icon';
import SearchLocation from '../../components/search/searchLocation';
import CustomModal from '../../components/CustomModal';
import config from '../../constants/config';
import Placeholder from '../../assets/images/Placeholder.png';

const AnnounceScreen = () => {
  const {t} = useTranslation();
  const {
    LoadAnnouncesByTag,
    SetHeader,
    SetAnnounceLocation,
    CheckActiveSession,
  } = useRematchDispatch((dispatch) => ({
    LoadAnnounces: dispatch.announce.LoadAnnounces,
    LoadAnnouncesByTag: dispatch.announce.LoadAnnouncesByTag,
    SetHeader: dispatch.preference.SetHeader,
    SetAnnounceLocation: dispatch.preference.SetAnnounceLocation,
    CheckActiveSession: dispatch.user.CheckActiveSession,
  }));
  const loadedData = useSelector((state) => state.announce.announceData).sort(
    (a, b) => {
      return Date.parse(b.createdAt) - Date.parse(a.createdAt);
    },
  );
  const isLoading = useSelector((state) => state.search.isLoading);
  const defaultPosition = useSelector(
    (state) => state.preference.announceLocation,
  );
  const [tag, setTag] = useState();
  const [radius, setRadius] = useState(50);
  const [location, setLocation] = useState();
  const [showDetail, setShowDetail] = useState(false);
  const [announceId, setAnnounceId] = useState();
  const [userRadius] = useState(false);
  const [showDefaultPositionChoice, setShowDefaultPositionChoice] = useState(
    false,
  );
  const [init, setInit] = useState();

  useFocusEffect(
    useCallback(() => {
      SetHeader({title: t('title_announce'), image: 'event-note'});
      CheckActiveSession();
      setInit(new Date());
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []),
  );

  useEffect(() => {
    if (location) {
      LoadAnnounceData();
    } else if (defaultPosition) {
      setLocation(defaultPosition);
    } else if (defaultPosition === null) {
      setShowDefaultPositionChoice(true);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [defaultPosition, init]);

  useEffect(() => {
    if (!location) {
      return;
    }
    LoadAnnounceData();
    if (!defaultPosition) {
      SetAnnounceLocation(location);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [location]);

  const handleKeyPress = () => {
    Keyboard.dismiss;
    LoadAnnounceData();
  };

  const LoadAnnounceData = () => {
    const townId = location ? location.id : '';
    LoadAnnouncesByTag({tag, townId});
  };

  const handleRadiusChange = (value) => {
    setRadius(value);
  };
  const handleLocationChange = (value) => {
    if (value) {
      setLocation(value);
    } else {
      setLocation(defaultPosition);
    }
  };

  const onPressItem = ({item}) => {
    setAnnounceId(item.id);
    setModalVisibility();
  };

  const onTagChange = (value) => {
    if (value) {
      setTag(value);
    } else {
      setTag();
    }
  };

  const setModalVisibility = () => {
    if (showDetail) {
      setAnnounceId();
    }
    setShowDetail(!showDetail);
  };

  const timeSinceAnnounceCreation = (createdAt) => {
    const diffTime = Math.abs(new Date() - new Date(createdAt));
    const diffDays = Math.trunc(diffTime / (1000 * 60 * 60 * 24));
    if (diffDays === 0) {
      const diffHours = Math.trunc(diffTime / (1000 * 60 * 60));
      if (diffHours === 0) {
        const diffMinutes = Math.trunc(diffTime / (1000 * 60));
        return `${diffMinutes} m`;
      }
      return `${diffHours} h`;
    }
    return `${diffDays} j`;
  };

  const renderSubtitle = (item) => (
    <View style={styles.listTitleContainer}>
      <Text style={styles.listTitle}>{`${item.title.toUpperCase()}`}</Text>
      <View style={styles.positionView}>
        <MaterialIcon style={styles.icon} name="location-on" size={15} />
        <Text style={styles.listSubTitle}>{`${item.location}`}</Text>
      </View>
      <Text style={[styles.listTitle, styles.price]}>{`${item.price}`}</Text>
    </View>
  );

  const renderItem = ({item}) => (
    <TouchableOpacity
      style={styles.itemBody}
      onPress={() => {
        onPressItem({item});
      }}>
      <ListItem
        title={renderSubtitle(item)}
        containerStyle={styles.listItem}
        badge={{
          value: timeSinceAnnounceCreation(item.createdAt),
          textStyle: styles.badgeText,
          containerStyle: styles.badgeContainer,
          badgeStyle: styles.badge,
        }}
        leftAvatar={{
          rounded: false,
          size: 'large',
          title: item.title.substr(0, 2),
          source: {
            uri:
              item.pictures?.length > 0
                ? item.pictures[0]
                : config.DEFAULT_IMAGE_URL,
          },
          avatarStyle: styles.avatarStyle,
        }}
      />
    </TouchableOpacity>
  );

  const renderX = () => (
    <FlatList
      style={styles.listContainer}
      data={loadedData}
      renderItem={renderItem}
      keyExtractor={(item) => item.id}
      ListEmptyComponent={<EmptyList message={t('search_empty_message')} />}
    />
  );

  const renderIcon = () => (
    <MaterialIcon name="search" size={25} color={Colors.royalblue} />
  );

  return (
    <>
      <CustomModal isVisible={showDetail} onCloseModal={setModalVisibility}>
        <AnnounceDetail onClose={setModalVisibility} announceId={announceId} />
      </CustomModal>
      <SafeAreaView style={styles.container}>
        <View style={styles.filtersContainer}>
          <EasySearchBar
            value={tag}
            onChangeText={onTagChange}
            onKeyPress={handleKeyPress}
            placeHolder={t('announce_search_placeholder')}
            onCancel={LoadAnnounceData}
            searchIcon={renderIcon}
          />
          {userRadius && (
            <EasySlider onChange={handleRadiusChange} label={t('radius')} />
          )}
          {!userRadius && <View style={styles.divider} />}
          <SearchLocation
            onChange={handleLocationChange}
            show={showDefaultPositionChoice}
            value={location?.location}
          />
        </View>

        {isLoading === true ? <SearchSkeleton value={5} /> : renderX()}
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.clearGray,
  },
  sliderContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 20,
  },
  sliderText: {
    fontFamily: FONTS.OpenSansItalic,
    fontSize: 12,
    marginHorizontal: 10,
  },
  slider: {
    width: '70%',
  },
  divider: {
    marginTop: 5,
  },
  listContainer: {
    backgroundColor: Colors.clearGray,
    paddingHorizontal: 10,
    paddingTop: 5,
  },
  listItem: {
    borderRadius: 10,
    margin: 0,
    padding: 5,
  },
  listTitleContainer: {
    flex: 1,
    justifyContent: 'space-between',
    flexDirection: 'column',
  },
  listTitle: {
    fontFamily: FONTS.OpenSansBold,
    color: Colors.royalblue,
    fontSize: 12,
    alignSelf: 'flex-start',
    marginLeft: 3,
  },
  listSubTitle: {
    fontFamily: FONTS.Avenir,
    color: Colors.darkGrey,
    fontSize: 10,
  },
  price: {
    marginTop: 0,
  },
  positionView: {
    flexDirection: 'row',
  },
  icon: {
    marginRight: 5,
    color: Colors.darkGrey,
  },
  badge: {
    backgroundColor: Colors.meanBlue,
  },
  badgeContainer: {
    backgroundColor: Colors.white,
    bottom: -20,
  },
  badgeText: {
    fontFamily: FONTS.OpenSans,
    color: Colors.white,
    fontSize: 10,
  },
  itemBody: {
    margin: 5,
    backgroundColor: Colors.white,
    borderRadius: 5,
    shadowColor: Colors.black,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    alignSelf: 'baseline',
    width: '97%',
  },
  filtersContainer: {
    backgroundColor: Colors.meanBlue,
    paddingHorizontal: 15,
    paddingBottom: 10,
    paddingTop: 10,
  },
  avatarStyle: {
    borderColor: Colors.clearGray,
    borderWidth: 1,
    margin: 0,
  },
});

export default AnnounceScreen;
