import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  SafeAreaView,
  View,
  FlatList,
  ActivityIndicator,
  Text,
  TouchableOpacity,
} from 'react-native';
import {useFocusEffect} from '@react-navigation/core';
import {useSelector} from 'react-redux';
import {Button} from 'react-native-elements';
import endOfMonth from 'date-fns/endOfMonth';
import startOfMonth from 'date-fns/startOfMonth';

//hooks
import {useTranslation} from '../../hooks/useTranslation';
import useRematchDispatch from '../../hooks/useRematchDispatch';

//services
import Colors from '../../constants/Colors';

//components
import MCalendar from '../../components/search/calendar';

import EmptyList from '../../components/emptyList';
import ROUTES from '../../navigation/Routes';
import FONTS from '../../constants/fonts';

import Feather from 'react-native-vector-icons/Feather';

const AppointmentScreen = (props) => {
  const {t} = useTranslation();
  const {officeId, serviceId, serviceName, location, customerName} = props.route.params;

  const {GetServiceCalendar, SetHeader} = useRematchDispatch((dispatch) => ({
    GetServiceCalendar: dispatch.calendar.GetServiceCalendar,
    SetHeader: dispatch.preference.SetHeader,
  }));
  const serviceCalendar = useSelector(
    (state) => state.calendar.serviceCalendar,
  );
  const dateWithAvailableTimeSlots = useSelector(
    (state) => state.calendar.dateWithAvailableTimeSlots,
  );

  const ongoing = useSelector((state) => state.calendar.ongoing);
  const navigator = useSelector((state) => state.preference.navigator);
  const [selectedDay, setSelectedDay] = useState();
  const [selectedDayTimeSlots, setSelectedDayTimeSlots] = useState([]);
  const [selectedTimeSlot, setSelectedTimeSlot] = useState(null);

  useFocusEffect(() => {
    SetHeader({
      title: t('header.date_hour'),
      image: 'timer',
      backTo: () =>
        props.navigation.navigate(ROUTES.appointmentNavigation.summary),
    });
  });

  useEffect(() => {
    if (navigator.account === true) {
      props.navigation.navigate(ROUTES.appointmentNavigation.search);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [navigator]);

  useEffect(() => {
    loadCalendar(new Date());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const loadCalendar = (day) => {
    const startDate = startOfMonth(day);
    const endDate = endOfMonth(day);
    GetServiceCalendar({officeId, serviceId, startDate, endDate});
  };
  const handleDayPress = (day) => {
    const dateString = day.dateString;
    setSelectedDay(day);
    const dayTimeSlots = serviceCalendar.find((sc) => sc.date === dateString);
    setSelectedDayTimeSlots(
      dayTimeSlots?.timeSlots.filter((ts) => ts.isAvailable === true) ?? [],
    );
  };
  const handleMonthChange = (month) => {
    setSelectedTimeSlot(null);
    setSelectedDay(null);
    if (endOfMonth(month.timestamp) > new Date()) {
      loadCalendar(month.timestamp);
    }
  };
  const onItemPress = (ts) => {
    if (ts.id === selectedTimeSlot?.id) {
      navigateToConfirmation();
    } else {
      setSelectedTimeSlot(ts);
    }
  };

  const navigateToConfirmation = () => {
    props.navigation.navigate(ROUTES.appointmentNavigation.confirmation, {
      serviceName,
      location,
      customerName,
      timeSlot: selectedTimeSlot,
      day: selectedDay,
    });
  };

  const renderItem = ({item}) => {
    if (item.id === selectedTimeSlot?.id) {
      return (
        <TouchableOpacity
          style={styles.buttonSelect}
          onPress={() => onItemPress(item)}>
          <Text style={styles.buttonText}>
            {t('common.buttonWithConfirmation')}
          </Text>
          <Text style={[styles.buttonText, {marginLeft: -40}]}>
            {item.startTime}
          </Text>
          <Feather name="check-circle" size={20} color={Colors.royalblue} />
        </TouchableOpacity>
      );
    }

    return (
      <Button
        title={item.startTime}
        onPress={() => onItemPress(item)}
        buttonStyle={[styles.buttonStyle]}
      />
    );
  };

  const renderList = () => (
    <View style={styles.listContainer}>
      <FlatList
        data={selectedDayTimeSlots}
        renderItem={renderItem}
        keyExtractor={(item) => item.id}
        ListEmptyComponent={<EmptyList message={t('no_time_slot')} />}
      />
    </View>
  );

  return (
    <SafeAreaView style={styles.container}>
      <View style={{alignItems: 'center'}}>
        {ongoing && <ActivityIndicator size="large" color={Colors.light} />}
        <MCalendar
          onDayPress={handleDayPress}
          selectedDay={selectedDay?.dateString}
          onMonthChange={handleMonthChange}
          dateWithAvailableTimeSlots={dateWithAvailableTimeSlots}
        />
      </View>
      {selectedDay ? (
        renderList()
      ) : (
        <EmptyList message={t('select_day_message')} />
      )}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.greyhish,
  },
  calenderContainer: {
    marginVertical: 10,
    marginHorizontal: 10,
    width: '90%',
  },
  calender: {
    borderRadius: 10,
  },
  listContainer: {
    margin: 10,
    flex: 1,
    width: '90%',
    alignSelf: 'center',
  },
  confirmation: {
    margin: 10,
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  buttonStyle: {
    margin: 5,
    borderColor: Colors.royalblue,
    backgroundColor: Colors.royalblue,
  },
  buttonTitleStyle: {
    fontFamily: FONTS.OpenSans,
    flex: 1,
    alignItems: 'center',
  },
  buttonSelect: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    flex: 1,
    margin: 5,
    borderColor: Colors.royalblue,
    borderRadius: 2,
    borderWidth: 0.2,
    paddingVertical: 8,
    paddingHorizontal: 5,
  },
  buttonText: {
    fontFamily: FONTS.OpenSans,
    color: Colors.meanBlue,
  },
});

export default AppointmentScreen;
