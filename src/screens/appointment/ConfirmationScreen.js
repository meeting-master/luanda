import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  SafeAreaView,
  Text,
  View,
  ActivityIndicator,
  ScrollView,
} from 'react-native';
import {useFocusEffect} from '@react-navigation/core';
import {Button, ListItem} from 'react-native-elements';
import {useSelector} from 'react-redux';

//hooks
import {useTranslation} from '../../hooks/useTranslation';
import useRematchDispatch from '../../hooks/useRematchDispatch';

//services
import Colors from '../../constants/Colors';
import FONTS from '../../constants/fonts';
import format from 'date-fns/format';
import ROUTES from '../../navigation/Routes';
import {IonIcon, MaterialIcon} from '../../components/Icon';
import Feather from 'react-native-vector-icons/Feather';

import CustomAlert from '../../components/CustomAlert';

const ConfirmationScreen = (props) => {
  const {t} = useTranslation();
  const {
    serviceName,
    location,
    address,
    customerName,
    timeSlot,
    day,
  } = props.route.params;
  const date = new Date(day.year, day.month - 1, day.day);
  const {CreateAppointment, SetHeader} = useRematchDispatch((dispatch) => ({
    CreateAppointment: dispatch.calendar.CreateAppointment,
    SetHeader: dispatch.preference.SetHeader,
  }));

  const ongoing = useSelector((state) => state.calendar.ongoing);
  const operationState = useSelector((state) => state.calendar.operationState);
  const currentUser = useSelector((state) => state.user.currentUser);
  const navigator = useSelector((state) => state.preference.navigator);
  const error = useSelector((state) => state.calendar.error);

  const [showAlert, setShowAlert] = useState(false);
  const [alertMessage, setAlertMessage] = useState('');

  useFocusEffect(() => {
    SetHeader({
      title: t('confirmation'),
      image: 'timer',
      backTo: () =>
        props.navigation.navigate(ROUTES.appointmentNavigation.appointment),
    });
  }, []);

  useEffect(() => {
    if (navigator.account === true) {
      props.navigation.navigate(ROUTES.appointmentNavigation.search);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [navigator]);

  useEffect(() => {
    if (operationState === null) {
      return;
    }
    let message = t('operation_success');
    if (operationState === false) {
      message = t('operation_error');
      if (error === 'auth/unvalidated-email') {
        message = t('common.fireBaseError.email-not-verified');
      }
    }
    setShowAlert(true);
    setAlertMessage(message);

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [operationState]);

  const handleOnConfirmPressed = () => {
    props.navigation.navigate(ROUTES.appointmentNavigation.search);
  };

  function handleSubmit() {
    const displayName = JSON.parse(currentUser.displayName);
    const displayNameWithEmail = JSON.stringify({
      ...displayName,
      email: currentUser.email,
    });
    CreateAppointment({
      owner: currentUser.uid,
      detail: displayNameWithEmail,
      placeNumber: 1,
      timeSlotId: timeSlot.id,
    });
  }

  const renderItem = (name, icon, type) => (
    <View style={styles.item}>
      <ListItem
        containerStyle={{margin: 0, padding: 10}}
        title={name}
        titleStyle={styles.listTitle}
        leftAvatar={
          type !== null ? (
            <IonIcon name={icon} size={22} color={Colors.royalblue} />
          ) : (
            <MaterialIcon name={icon} size={22} color={Colors.royalblue} />
          )
        }
      />
    </View>
  );
  return (
    <SafeAreaView style={styles.container}>
      {ongoing && <ActivityIndicator size="large" color={Colors.light} />}
      <View style={styles.band}>
        <Text style={styles.bandText}>{t('header.confirmation')}</Text>
      </View>
      <ScrollView style={styles.body}>
        {renderItem(customerName, 'business')}
        {renderItem(serviceName, 'flag')}
        {renderItem(address, 'locate')}
        {renderItem(location, 'location')}
        {renderItem(format(date, t('date_format')), 'calendar')}
        {renderItem(`${timeSlot.startTime} - ${timeSlot.endTime}`, 'timer')}
        <Button
          onPress={handleSubmit}
          title={t('confirmation_text')}
          buttonStyle={styles.buttonStyle}
          titleStyle={styles.buttonTitleStyle}
          icon={
            <Feather
              name="check-circle"
              size={25}
              color={Colors.white}
              style={styles.buttonIcon}
            />
          }
        />
      </ScrollView>
      <CustomAlert
        message={alertMessage}
        showAlert={showAlert}
        confirmText="OK"
        onCancelPressed={() => {
          setShowAlert(false);
        }}
        onConfirmPressed={() => {
          handleOnConfirmPressed();
        }}
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  body: {
    margin: 10,
    backgroundColor: Colors.white,
    borderRadius: 5,
    shadowColor: Colors.black,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    alignSelf: 'baseline',
    width: '95%',
  },

  band: {
    width: '100%',
    height: 50,
    backgroundColor: Colors.meanBlue,
    justifyContent: 'center',
    marginTop: 5,
    paddingLeft: 20,
  },
  bandText: {
    fontFamily: FONTS.OpenSans,
    color: Colors.white,
  },
  listTitle: {
    color: Colors.royalblue,
    fontFamily: FONTS.OpenSans,
    fontSize: 11,
  },
  buttonStyle: {
    margin: 5,
    marginTop: 20,
    backgroundColor: Colors.royalblue,
  },
  buttonTitleStyle: {
    fontFamily: FONTS.OpenSans,
    fontSize: 12,
  },
  buttonIcon: {
    marginHorizontal: 15,
  },
  item: {paddingLeft: 3},
});

export default ConfirmationScreen;
