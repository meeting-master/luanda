import React, {useCallback, useEffect, useState} from 'react';
import {
  FlatList,
  Keyboard,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {useFocusEffect} from '@react-navigation/core';
import {useSelector} from 'react-redux';
import {ListItem} from 'react-native-elements';
import Geolocation from '@react-native-community/geolocation';

//hooks
import {useTranslation} from '../../hooks/useTranslation';
import useRematchDispatch from '../../hooks/useRematchDispatch';

//Services
import Colors from '../../constants/Colors';
import FONTS from '../../constants/fonts';

//components
import SearchSkeleton from '../../components/search/skeleton';
import EmptyList from '../../components/emptyList';
import Ratingg from '../../components/rating';
import EasySlider from '../../components/Slider';
import EasySearchBar from '../../components/search/searchBar';
import {MaterialIcon} from '../../components/Icon';
import ROUTES from '../../navigation/Routes';
import SearchLocation from '../../components/search/searchLocation';

const SearchScreen = (props) => {
  const {t} = useTranslation();
  // toDo: replace with a transparent image
  const defaultLogoUrl =
    'https://us.123rf.com/450wm/mathier/mathier1905/mathier190500001/134557215-no-thumbnail-images-placeholder-for-forums-blogs-and-websites.jpg?ver=6';

  const {
    LoadSearchData,
    SetHeader,
    setOffice,
    SetBackTo,
    ClearBackTo,
    SetNavigator,
    SetAppointmentLocation,
    CheckActiveSession,
  } = useRematchDispatch((dispatch) => ({
    LoadInitial: dispatch.search.LoadInitial,
    LoadSearchData: dispatch.search.LoadSearchData,
    SetHeader: dispatch.preference.SetHeader,
    setOffice: dispatch.search.setOffice,
    SetBackTo: dispatch.preference.SetBackTo,
    ClearBackTo: dispatch.preference.ClearBackTo,
    SetNavigator: dispatch.preference.SetNavigator,
    SetAppointmentLocation: dispatch.preference.SetAppointmentLocation,
    CheckActiveSession: dispatch.user.CheckActiveSession,
  }));
  const loadedData = useSelector((state) => state.search.searchData);
  const isLoading = useSelector((state) => state.search.isLoading);
  const defaultPosition = useSelector(
    (state) => state.preference.appointmentLocation,
  );
  const currentUser = useSelector((state) => state.user.currentUser);

  const [tag, setTag] = useState();
  const [radius, setRadius] = useState(50);
  const [userRadius] = useState(false);
  const [location, setLocation] = useState();
  const [position, setPosition] = useState();
  const [showDefaultPositionChoice, setShowDefaultPositionChoice] = useState(
    false,
  );

  useFocusEffect(
    useCallback(() => {
      SetHeader({title: t('label_search'), image: 'timer'});
      CheckActiveSession();
      SetNavigator({announce: false, appointment: false, account: false});
      Geolocation.getCurrentPosition((pos) => {
        setPosition(pos.coords);
      });
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []),
  );

  useEffect(() => {
    ClearBackTo();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (defaultPosition) {
      setLocation(defaultPosition);
    } else if (defaultPosition === null) {
      setShowDefaultPositionChoice(true);
    }
  }, [defaultPosition]);

  useEffect(() => {
    LoadData();
    if (!defaultPosition) {
      SetAppointmentLocation(location);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [location, position]);

  const onTagChange = (value) => {
    if (value) {
      setTag(value);
    } else {
      setTag();
    }
  };

  const handleKeyPress = () => {
    Keyboard.dismiss;
    LoadData();
  };
  const LoadData = () => {
    LoadSearchData({
      tag,
      townId: location ? location.id : '',
      lat: position?.latitude,
      lon: position?.longitude,
    });
  };

  const handleRadiusChange = (value) => {
    setRadius(value);
  };

  const onPressItem = ({item}) => {
    setOffice(item);
    if (currentUser === null) {
      SetBackTo(ROUTES.appointmentNavigation.summary);
      props.navigation.navigate(ROUTES.login);
    } else if (
      currentUser.displayName === null ||
      currentUser.displayName === ''
    ) {
      SetBackTo(ROUTES.appointmentNavigation.summary);
      props.navigation.navigate(ROUTES.profile);
    } else {
      props.navigation.navigate(ROUTES.appointmentNavigation.summary);
    }
  };

  const handleLocationChange = (value) => {
    if (value) {
      setLocation(value);
    } else {
      setLocation(defaultPosition);
    }
  };

  const renderSubtitle = (item) => (
    <View style={styles.listTitleContainer}>
      <Text style={styles.listTitle}>{`${item.customerName}`}</Text>
      <Text style={styles.listActivites}>{`${item.activities}`}</Text>
      <View style={styles.positionView}>
        <MaterialIcon style={styles.icon} name="location-on" size={15} />
        <Text style={styles.location}>{`${item.location}`}</Text>
      </View>
      <Ratingg value={item.rating} />
    </View>
  );

  const renderBadge = (distance) => ({
    value: `${distance ? distance : 0} km`,
    textStyle: styles.badgeText,
    containerStyle: styles.badgeContainer,
    badgeStyle: styles.badge,
  });

  const renderAvatar = (name, logoUrl) => ({
    rounded: false,
    size: 'large',
    title: name.substr(0, 2),
    source: {
      uri: logoUrl ? logoUrl : defaultLogoUrl,
    },
    avatarStyle: styles.avatarStyle,
  });

  const renderItem = ({item}) => (
    <TouchableOpacity
      style={styles.itemBody}
      onPress={() => {
        onPressItem({item});
      }}>
      <ListItem
        title={renderSubtitle(item)}
        chevron
        badge={item.distance ? renderBadge(item.distance) : null}
        leftAvatar={renderAvatar(item.name, item.logoUrl)}
      />
    </TouchableOpacity>
  );

  const renderX = () => (
    <FlatList
      style={styles.listContainer}
      data={loadedData}
      renderItem={renderItem}
      keyExtractor={(item) => item.id}
      ListEmptyComponent={<EmptyList message={t('search_empty_message')} />}
    />
  );

  const renderIcon = () => (
    <MaterialIcon name="search" size={25} color={Colors.royalblue} />
  );

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.filtersContainer}>
        <EasySearchBar
          value={tag}
          onChangeText={onTagChange}
          onKeyPress={handleKeyPress}
          placeHolder={t('appointment_search_placeholder')}
          onCancel={LoadData}
          searchIcon={renderIcon}
        />

        {userRadius && (
          <EasySlider onChange={handleRadiusChange} label={t('radius')} />
        )}
        {!userRadius && <View style={styles.divider} />}
        <SearchLocation
          onChange={handleLocationChange}
          show={showDefaultPositionChoice}
          value={location?.location}
        />
      </View>
      {isLoading === true ? <SearchSkeleton value={5} /> : renderX()}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.clearGray,
  },
  sliderContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 20,
  },
  sliderText: {
    fontFamily: FONTS.OpenSansItalic,
    fontSize: 12,
    marginHorizontal: 10,
  },
  slider: {
    width: '70%',
  },
  divider: {
    marginTop: 5,
  },
  filtersContainer: {
    backgroundColor: Colors.meanBlue,
    paddingHorizontal: 15,
    paddingBottom: 10,
    paddingTop: 10,
  },
  icon: {
    marginRight: 5,
    color: Colors.darkGrey,
  },
  listContainer: {
    backgroundColor: Colors.clearGray,
    paddingHorizontal: 10,
    paddingTop: 5,
  },
  listItem: {
    marginVertical: 5,
    height: 80,
  },
  itemBody: {
    margin: 5,
    backgroundColor: Colors.white,
    borderRadius: 5,
    shadowColor: Colors.black,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    alignSelf: 'baseline',
    width: '97%',
  },
  avatarStyle: {
    borderColor: Colors.royalblue,
    borderWidth: 0.5,
    margin: 0,
  },
  listTitleContainer: {
    flex: 1,
    justifyContent: 'space-between',
    flexDirection: 'column',
  },
  listTitle: {
    fontFamily: FONTS.OpenSansBold,
    color: Colors.royalblue,
    fontSize: 11,
    paddingLeft: 3,
  },
  listActivites: {
    fontFamily: FONTS.OpenSans,
    color: Colors.royalblue,
    fontSize: 11,
    marginBottom: 3,
    paddingLeft: 3,
  },
  positionView: {
    flexDirection: 'row',
    marginBottom: 0,
  },
  location: {
    fontFamily: FONTS.OpenSans,
    color: Colors.darkGrey,
    fontSize: 9,
  },
  badgeContainer: {
    backgroundColor: Colors.white,
    bottom: -20,
  },
  badgeText: {
    fontFamily: FONTS.OpenSans,
    color: Colors.white,
    fontSize: 9,
  },
  badge: {
    backgroundColor: Colors.meanBlue,
  },
});

export default SearchScreen;
