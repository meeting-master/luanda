import React, {useEffect, useState} from 'react';
import {
  FlatList,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {useFocusEffect} from '@react-navigation/core';
import {ListItem} from 'react-native-elements';

//hooks
import {useTranslation} from '../../hooks/useTranslation';
import useRematchDispatch from '../../hooks/useRematchDispatch';

//services
import Colors from '../../constants/Colors';
import FONTS from '../../constants/fonts';
import {useSelector} from 'react-redux';

//components
import SearchSkeleton from '../../components/search/skeleton';
import Comment from '../../components/comment';
import {MaterialIcon} from '../../components/Icon';
import CustomModal from '../../components/CustomModal';
import ROUTES from '../../navigation/Routes';
import Ratingg from '../../components/rating';
import Octicons from 'react-native-vector-icons/Octicons';

const ServiceScreen = ({navigation}) => {
  const {t} = useTranslation();
  const defaultLogoUrl =
    'https://us.123rf.com/450wm/mathier/mathier1905/mathier190500001/134557215-no-thumbnail-images-placeholder-for-forums-blogs-and-websites.jpg?ver=6';

  const {
    LoadOfficeRatingComments,
    LoadOfficeServices,
    SetHeader,
  } = useRematchDispatch((dispatch) => ({
    SetHeader: dispatch.preference.SetHeader,
    LoadOfficeServices: dispatch.search.LoadOfficeServices,
    LoadOfficeRatingComments: dispatch.search.LoadOfficeRatingComments,
  }));
  const loadedServices = useSelector((state) => state.search.officeServices);
  const loadedRatingComments = useSelector(
    (state) => state.search.ratingComments,
  );
  const isLoading = useSelector((state) => state.search.isLoading);
  const office = useSelector((state) => state.search.office);
  const services = useSelector((state) => state.search.services);
  const navigator = useSelector((state) => state.preference.navigator);
  const [modalVisible, setModalVisible] = useState(false);

  useFocusEffect(() => {
    SetHeader({
      title: t('header.appointment'),
      image: 'timer',
      backTo: () => navigation.navigate(ROUTES.appointmentNavigation.search),
    });
  });

  useEffect(() => {
    if (navigator.appointment === true) {
      navigation.navigate(ROUTES.appointmentNavigation.search);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [navigator]);

  useEffect(() => {
    if (office) {
      LoadOfficeServices(office.id);
      LoadOfficeRatingComments(office.id);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [office]);

  const onPressItem = (item) => {
    navigation.navigate(ROUTES.appointmentNavigation.appointment, {
      serviceId: item.id,
      serviceName: item.name,
      address: `${office?.streetNumber} ${office?.streetName}`,
      location: office?.location,
      customerName: office?.customerName,
      officeId: office.id,
    });
  };

  const renderItem = ({item}) => (
    <TouchableOpacity onPress={() => onPressItem(item)}>
      <ListItem
        title={item.name}
        titleStyle={styles.listTitle}
        chevron
        containerStyle={styles.listItem}
      />
    </TouchableOpacity>
  );

  const renderServices = () => (
    <FlatList
      style={styles.listContainer}
      data={loadedServices}
      renderItem={renderItem}
      keyExtractor={(item) => item.id}
    />
  );

  const setModalVisibility = () => setModalVisible(!modalVisible);

  const renderAvatar = (name, logoUrl) => ({
    rounded: false,
    size: 'large',
    title: name.substr(0, 2),
    source: {
      uri: logoUrl ? logoUrl : defaultLogoUrl,
    },
    avatarStyle: styles.avatarStyle,
    containerStyle: {padding: 0},
  });

  const renderSubtitle = (item) => (
    <View style={styles.headerListTitleContainer}>
      <Text style={styles.headerListTitle}>{`${item.customerName}`}</Text>
      <View style={styles.positionView}>
        <MaterialIcon style={styles.icon} name="location-on" size={15} />
        <Text style={styles.location}>{`${item?.location}`}</Text>
      </View>
      <Ratingg value={item.rating} />
    </View>
  );

  const renderHeader = () => (
    <ListItem
      title={office && renderSubtitle(office)}
      containerStyle={styles.headerItem}
      leftAvatar={office && renderAvatar(office.name, office.logoUrl)}
      rightIcon={
        <TouchableOpacity onPress={setModalVisibility} style={styles.comments}>
          <Text style={styles.commentText}>{t('comments')}</Text>
          <Octicons name="comment" size={20} color={Colors.meanBlue} />
        </TouchableOpacity>
      }
    />
  );

  return (
    <>
      <CustomModal onCloseModal={setModalVisibility} isVisible={modalVisible}>
        <Comment onClose={setModalVisibility} comments={loadedRatingComments} />
      </CustomModal>
      <SafeAreaView style={styles.container}>
        <View style={styles.headerContainer}>{renderHeader()}</View>
        <View style={styles.band}>
          <Text style={styles.bandText}>{t('offered_services')}</Text>
        </View>
        <View style={styles.listContainer}>
          {isLoading === true ? <SearchSkeleton value={3} /> : renderServices()}
        </View>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: Colors.white,
  },

  headerContainer: {
    width: '100%',
    height: 70,
  },

  headerListTitleContainer: {
    flex: 1,
    justifyContent: 'space-between',
    flexDirection: 'column',
  },
  headerListTitle: {
    fontFamily: FONTS.OpenSans,
    color: Colors.royalblue,
    fontSize: 11,
    marginBottom: 3,
    paddingLeft: 3,
  },
  positionView: {
    flexDirection: 'row',
    marginBottom: 0,
  },
  headerItem: {
    margin: 0,
  },
  headerSubTitle: {
    marginTop: 0,
    padding: 0,
  },
  band: {
    width: '100%',
    height: 50,
    backgroundColor: Colors.meanBlue,
    justifyContent: 'center',
    paddingLeft: 20,
    marginTop: 40,
  },
  bandText: {
    fontFamily: FONTS.OpenSans,
    color: Colors.white,
  },
  serviceText: {
    fontFamily: FONTS.OpenSans,
    fontSize: 16,
    color: Colors.dark,
    textAlign: 'center',
  },
  listItem: {
    marginVertical: 8,
    marginHorizontal: 16,
    color: Colors.royalblue,
    borderRadius: 5,
    fontFamily: FONTS.OpenSans,
    fontSize: 16,
    shadowColor: Colors.black,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  listTitle: {
    fontFamily: FONTS.OpenSansBold,
    color: Colors.meanBlue,
    fontSize: 14,
  },
  comments: {
    alignSelf: 'flex-end',
    flexDirection: 'row',
    alignItems: 'center',
    alignContent: 'center',
  },
  commentText: {
    fontFamily: FONTS.OpenSans,
    color: Colors.meanBlue,
    marginHorizontal: 10,
    fontSize: 11,
  },
  avatarStyle: {
    borderColor: Colors.royalblue,
    borderWidth: 0.5,
    margin: 0,
    padding: 0,
  },
  headerTitle: {
    fontFamily: FONTS.OpenSansBold,
    color: Colors.royalblue,
    fontSize: 12,
    marginBottom: 0,
    paddingLeft: 3,
  },

  location: {
    fontFamily: FONTS.OpenSans,
    color: Colors.darkGrey,
    fontSize: 10,
  },

  icon: {
    marginRight: 5,
    color: Colors.darkGrey,
  },
  listContainer: {
    width: '100%',
    marginTop: 2,
  },
});

export default ServiceScreen;
