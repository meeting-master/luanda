import React, {useCallback} from 'react';
import {StyleSheet, SafeAreaView, Text, TouchableOpacity} from 'react-native';

import {useTranslation} from '../../hooks/useTranslation';
import {ListItem} from 'react-native-elements';
import {IonIcon} from '../../components/Icon';
import Colors from '../../constants/Colors';
import ROUTES from '../../navigation/Routes';
import FONTS from '../../constants/fonts';
import useRematchDispatch from '../../hooks/useRematchDispatch';
import useFocusEffect from '@react-navigation/core/src/useFocusEffect';
import config from '../../constants/config';

const AboutScreen = ({navigation, route}) => {
  const {t} = useTranslation();
  const {SetHeader} = useRematchDispatch((dispatch) => ({
    SetHeader: dispatch.preference.SetHeader,
  }));

  useFocusEffect(
    useCallback(() => {
      SetHeader({
        title: t('header.about'),
        image: 'person-outline',
        backTo: () => navigation.navigate(ROUTES.root),
      });
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []),
  );

  const renderItem = (destination, name, icon, type, disabled = false) => (
    <TouchableOpacity
      disabled={disabled}
      onPress={() => {
        navigation.navigate(destination);
      }}>
      <ListItem
        containerStyle={styles.groupContainer}
        title={name.toUpperCase()}
        titleStyle={styles.groupText}
        bottomDivider
        chevron
        leftAvatar={
          type !== null ? (
            <IonIcon
              name={icon}
              size={20}
              color={disabled ? Colors.grey : Colors.primary}
            />
          ) : null
        }
      />
    </TouchableOpacity>
  );

  return (
    <SafeAreaView style={styles.container}>
      {renderItem(ROUTES.cgu, t('cgu'), 'note', null)}
      <Text style={styles.app}> {t('app_text')} </Text>
      <Text style={styles.version}> Version: {config.VERSION} </Text>
      <Text style={styles.company}>{t('company_text')} </Text>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.defaultBackground,
  },
  groupContainer: {
    backgroundColor: Colors.meanBlue,
    color: Colors.white,
    fontSize: 13,
  },
  groupText: {
    color: Colors.white,
    fontSize: 14,
    fontFamily: FONTS.OpenSansBold,
  },
  bg: {
    backgroundColor: Colors.greyhish,
  },
  listTitle: {
    fontFamily: FONTS.OpenSans,
    color: Colors.dark,
    fontSize: 12,
  },
  disabledListTitle: {
    fontFamily: FONTS.OpenSans,
    color: Colors.grey,
    fontSize: 12,
  },
  enabledListTitle: {
    color: Colors.dark,
  },

  app: {
    margin: 20,
    color: Colors.dark,
    fontFamily: FONTS.OpenSans,
  },
  version: {
    margin: 20,
    color: Colors.dark,
    fontFamily: FONTS.OpenSansItalic,
  },
  company: {
    margin: 20,
    color: Colors.dark,
    fontFamily: FONTS.OpenSansBold,
  },
});

export default AboutScreen;
