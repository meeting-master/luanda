import React, {useCallback} from 'react';
import {StyleSheet, SafeAreaView} from 'react-native';

import {useTranslation} from '../../hooks/useTranslation';
import Colors from '../../constants/Colors';
import FONTS from '../../constants/fonts';
import useFocusEffect from '@react-navigation/core/src/useFocusEffect';
import ROUTES from '../../navigation/Routes';
import useRematchDispatch from '../../hooks/useRematchDispatch';
import WebView from 'react-native-webview';

const CGUScreen = ({navigation, route}) => {
  const {t} = useTranslation();
  const {SetHeader} = useRematchDispatch((dispatch) => ({
    SetHeader: dispatch.preference.SetHeader,
  }));
  useFocusEffect(
    useCallback(() => {
      SetHeader({
        title: t('header.cgu'),
        image: 'settings',
        backTo: () => navigation.navigate(ROUTES.about),
      });
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []),
  );

  return <WebView source={{uri: 'https://easymeeting.io/cgu.html'}} />;
};

export default CGUScreen;
