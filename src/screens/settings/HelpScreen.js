import React, {useCallback} from 'react';
import {StyleSheet, SafeAreaView, ScrollView, Text} from 'react-native';

import {useTranslation} from '../../hooks/useTranslation';
import {ListItem} from 'react-native-elements';
import Colors from '../../constants/Colors';
import FONTS from '../../constants/fonts';
import useRematchDispatch from '../../hooks/useRematchDispatch';
import useFocusEffect from '@react-navigation/core/src/useFocusEffect';
import ROUTES from '../../navigation/Routes';

const HelpScreen = ({navigation}) => {
  const {t} = useTranslation();
  const {SetHeader} = useRematchDispatch((dispatch) => ({
    SetHeader: dispatch.preference.SetHeader,
  }));

  useFocusEffect(
    useCallback(() => {
      SetHeader({
        title: t('header.help'),
        image: 'help',
        backTo: () => navigation.navigate(ROUTES.root),
      });
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []),
  );

  const renderGroup = (name) => (
    <ListItem
      title={name.toUpperCase()}
      bottomDivider
      topDivider
      containerStyle={styles.groupContainer}
      titleStyle={styles.groupText}
    />
  );
  return (
    <SafeAreaView style={styles.container}>
      <ScrollView>
        {renderGroup(t('help.create_account'))}
        <Text style={styles.text}>{t('help.create_account_text')}</Text>
        {renderGroup(t('help.create_announce'))}
        <Text style={styles.text}>{t('help.create_announce_text')}</Text>
        {renderGroup(t('help.change_announce'))}
        <Text style={styles.text}>{t('help.change_announce_text')}</Text>
        {renderGroup(t('help.create_appointment'))}
        <Text style={styles.text}>{t('help.create_appointment_text')}</Text>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.defaultBackground,
  },
  groupContainer: {
    backgroundColor: Colors.meanBlue,
    color: Colors.white,
    fontSize: 13,
  },
  groupText: {
    color: Colors.white,
    fontSize: 14,
    fontFamily: FONTS.OpenSansBold,
  },
  bg: {
    backgroundColor: Colors.greyhish,
  },
  listTitle: {
    fontFamily: FONTS.OpenSans,
    color: Colors.dark,
    fontSize: 12,
  },
  disabledListTitle: {
    fontFamily: FONTS.OpenSans,
    color: Colors.grey,
    fontSize: 12,
  },
  enabledListTitle: {
    color: Colors.dark,
  },
  text: {
    margin: 20,
    color: Colors.dark,
    fontFamily: FONTS.OpenSans,
  },
});

export default HelpScreen;
