import React, {useCallback, useEffect, useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  KeyboardAvoidingView,
  Platform,
} from 'react-native';
import {useSelector} from 'react-redux';
import {useFocusEffect} from '@react-navigation/core';
import messaging from '@react-native-firebase/messaging';
import AsyncStorage from '@react-native-community/async-storage';

//hooks
import useRematchDispatch from '../../hooks/useRematchDispatch';
import {useTranslation} from '../../hooks/useTranslation';
import {useForm, Controller} from 'react-hook-form';

//components
import SearchSkeleton from '../../components/search/skeleton';
import Colors from '../../constants/Colors';
import ROUTES from '../../navigation/Routes';
import CustomAlert from '../../components/CustomAlert';

const LoginScreen = ({navigation, props}) => {
  const {t} = useTranslation();
  const {
    login,
    clearErrorMessage,
    resetPassword,
    resetOperationWellDoneMessage,
    SetHeader,
    SetFCMToken,
  } = useRematchDispatch((dispatch) => ({
    login: dispatch.user.login,
    SetFCMToken: dispatch.user.SetFCMToken,
    clearErrorMessage: dispatch.user.clearErrorMessage,
    resetPassword: dispatch.user.resetPassword,
    resetOperationWellDoneMessage: dispatch.user.resetOperationWellDoneMessage,
    SetHeader: dispatch.preference.SetHeader,
  }));

  const currentUser = useSelector((state) => state.user.currentUser);
  const isLoading = useSelector((state) => state.user.isLoading);
  const errorMessage = useSelector((state) => state.user.errorMessage);
  const operationWellDoneMessage = useSelector(
    (state) => state.user.operationWellDoneMessage,
  );
  const backTo = useSelector((state) => state.preference.backTo);

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [showAlert, setShowAlert] = useState(false);
  const [alertMessage, setAlertMessage] = useState();

  const {control, handleSubmit, errors, formState, setValue} = useForm({
    mode: 'onChange',
    shouldUnregister: false,
  });

  useFocusEffect(
    useCallback(() => {
      SetHeader({
        title: t('header.login'),
        image: 'person-outline',
        backTo: () => navigation.navigate(ROUTES.root),
      });
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []),
  );

  useEffect(() => {
    return () => {
      clearErrorMessage();
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (currentUser !== null) {
      requestUserPermission();
      if (currentUser.displayName === null || currentUser.displayName === '') {
        navigation.navigate(ROUTES.profile, {backTo: backTo});
      } else {
        navigation.navigate(backTo ? backTo : ROUTES.root);
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [currentUser]);

  useEffect(() => {
    if (errorMessage !== '') {
      setValue('email', email);
      setValue('password', password);
      setShowAlert(true);
      switch (errorMessage) {
        case 'auth/email-not-verified':
          setAlertMessage(t('common.fireBaseError.email-not-verified'));
          break;
        case 'auth/user-not-found':
        case 'auth/wrong-password':
          setAlertMessage(t('common.fireBaseError.email-or-passord'));
          break;
        default:
      }
    } else if (operationWellDoneMessage !== '') {
      setEmail('');
      setPassword('');
      setShowAlert(true);
      setAlertMessage(t(operationWellDoneMessage));
      resetOperationWellDoneMessage();
    }
    clearErrorMessage();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [errorMessage, operationWellDoneMessage]);

  const requestUserPermission = async () => {
    const authStatus = await messaging().requestPermission();
    const enabled =
      authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
      authStatus === messaging.AuthorizationStatus.PROVISIONAL;

    if (enabled) {
      getToken();
    }
  };

  const getToken = async () => {
    let fcmToken = await AsyncStorage.getItem('fcmToken');
    if (!fcmToken) {
      fcmToken = await messaging().getToken();
      if (fcmToken) {
        SetFCMToken(fcmToken);
        await AsyncStorage.setItem('fcmToken', fcmToken);
      }
    }
  };

  const handleOnPress = (data) => {
    login(data);
  };

  const handleResetPassword = () => {
    if (email === '' || errors.email) {
      setAlertMessage(t('common.errors.email'));
      setShowAlert(true);
    } else {
      resetPassword({email: email});
    }
  };

  if (isLoading) {
    return <SearchSkeleton value={3} />;
  }
  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      style={{flex: 1}}>
      <View style={styles.container}>
        {errors.email && (
          <Text style={styles.errorText}>{t('common.errors.email')}</Text>
        )}
        <Controller
          control={control}
          render={({onChange, onBlur, value}) => (
            <TextInput
              keyboardType={'email-address'}
              style={styles.inputStyle}
              placeholder={t('email')}
              onBlur={onBlur}
              onChangeText={(value) => {
                onChange(value);
                setEmail(value);
              }}
              value={value}
            />
          )}
          name="email"
          rules={{
            required: true,
            pattern: /^[\w-\.\+]+@([\w-]+\.)+[\w-]{2,4}$/g,
          }}
          defaultValue=""
        />

        {errors.password && (
          <Text style={styles.errorText}>{t('common.errors.password')}</Text>
        )}
        <Controller
          control={control}
          render={({onChange, onBlur, value}) => (
            <TextInput
              style={styles.inputStyle}
              placeholder={t('password')}
              maxLength={15}
              secureTextEntry={true}
              onBlur={onBlur}
              onChangeText={(value) => {
                onChange(value);
                setPassword(value);
              }}
              value={value}
            />
          )}
          name="password"
          rules={{required: true}}
          defaultValue=""
        />

        <Button
          color={Colors.primary}
          title={t('sign_in')}
          disabled={!formState.isValid}
          onPress={handleSubmit(handleOnPress)}
        />

        <Text style={styles.loginText} onPress={() => handleResetPassword()}>
          {t('loginScreen.reset_password')}
        </Text>

        <Text
          style={styles.loginText}
          onPress={() => navigation.navigate(ROUTES.register)}>
          {t('not_account')}
        </Text>
        {alertMessage && showAlert && (
          <CustomAlert
            message={alertMessage}
            showAlert={showAlert}
            confirmText="OK"
            onCancelPressed={() => {
              setShowAlert(false);
            }}
            onConfirmPressed={() => {
              setShowAlert(false);
              setAlertMessage();
            }}
          />
        )}
      </View>
    </KeyboardAvoidingView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    padding: 35,
    backgroundColor: '#fff',
  },
  inputStyle: {
    width: '100%',
    marginBottom: 15,
    paddingBottom: 15,
    alignSelf: 'center',
    borderColor: Colors.primary,
    borderBottomWidth: 1,
  },
  loginText: {
    color: Colors.primary,
    marginTop: 25,
    textAlign: 'center',
  },
  errorText: {
    color: Colors.red,
  },
  preloader: {
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff',
  },
});

export default LoginScreen;
