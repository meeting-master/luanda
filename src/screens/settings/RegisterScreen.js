import React, {useCallback, useEffect, useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  KeyboardAvoidingView,
  Platform,
} from 'react-native';
import {useSelector} from 'react-redux';

//hooks
import useRematchDispatch from '../../hooks/useRematchDispatch';
import {useTranslation} from '../../hooks/useTranslation';
import {useForm, Controller} from 'react-hook-form';

//components
import SearchSkeleton from '../../components/search/skeleton';
import Colors from '../../constants/Colors';
import ROUTES from '../../navigation/Routes';
import useFocusEffect from '@react-navigation/core/src/useFocusEffect';
import CustomAlert from '../../components/CustomAlert';

const RegisterScreen = ({navigation, props}) => {
  const backTo = props ? props.route.params.backTo : null;
  const {t} = useTranslation();

  const {
    Register,
    clearErrorMessage,
    clearRegisterUser,
    SetHeader,
  } = useRematchDispatch((dispatch) => ({
    Register: dispatch.user.Register,
    clearErrorMessage: dispatch.user.clearErrorMessage,
    clearRegisterUser: dispatch.user.clearRegisterUser,
    SetHeader: dispatch.preference.SetHeader,
  }));
  const registerUser = useSelector((state) => state.user.registerUser);
  const isLoading = useSelector((state) => state.user.isLoading);
  const errorMessage = useSelector((state) => state.user.errorMessage);

  const {control, handleSubmit, errors, formState, getValues} = useForm({
    mode: 'onChange',
    shouldUnregister: false,
  });

  const [showAlert, setShowAlert] = useState(false);
  const [alertMessage, setAlertMessage] = useState();

  useFocusEffect(
    useCallback(() => {
      SetHeader({
        title: t('header.register'),
        image: 'person-outline',
        backTo: () => navigation.navigate(ROUTES.login),
      });
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []),
  );

  useEffect(() => {
    return () => {
      clearErrorMessage();
      clearRegisterUser();
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (registerUser !== null) {
      setAlertMessage(
        `${t('registerScreen.successful_creation_1')} ${t(
          'registerScreen.successful_creation_2',
        )}`,
      );
      setShowAlert(true);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [registerUser]);

  useEffect(() => {
    if (errorMessage !== '') {
      switch (errorMessage) {
        case 'auth/email-already-in-use':
          setAlertMessage(t('common.fireBaseError.email-already-in-use'));
          setShowAlert(true);
          break;
        case 'auth/invalid-email':
          setShowAlert(true);
          setAlertMessage(t('common.fireBaseError.invalid-email'));
          break;
        default:
      }
      clearErrorMessage();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [errorMessage]);

  const handleOnPress = (data) => {
    Register(data);
  };

  const validatePwdConfirmation = () => {
    return getValues('confirmPassword') === getValues('password');
  };

  const handleOnConfirmPressed = () => {
    setShowAlert(false);
    setAlertMessage();
    if (registerUser !== null) {
      navigation.navigate(ROUTES.login, {
        ...(backTo && {backTo: backTo}),
      });
    }
  };

  if (isLoading) {
    return <SearchSkeleton value={3} />;
  }
  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      style={{flex: 1}}>
      <View style={styles.container}>
        {errors.email && (
          <Text style={styles.errorText}>{t('common.errors.email')}</Text>
        )}
        <Controller
          control={control}
          render={({onChange, onBlur, value}) => (
            <TextInput
              keyboardType={'email-address'}
              style={styles.inputStyle}
              placeholder={t('email')}
              onBlur={onBlur}
              onChangeText={(value) => onChange(value)}
              value={value}
            />
          )}
          name="email"
          rules={{
            required: true,
            pattern: /^[\w-\.\+]+@([\w-]+\.)+[\w-]{2,4}$/g,
          }}
          defaultValue=""
        />

        {errors.password && errors.password.type === 'required' && (
          <Text style={styles.errorText}>{t('common.errors.password')}</Text>
        )}
        {errors.password && errors.password.type === 'minLength' && (
          <Text style={styles.errorText}>
            {t('common.errors.passwordWeak')}
          </Text>
        )}
        <Controller
          control={control}
          render={({onChange, onBlur, value}) => (
            <TextInput
              style={styles.inputStyle}
              placeholder={t('password')}
              maxLength={15}
              secureTextEntry={true}
              onBlur={onBlur}
              onChangeText={(value) => onChange(value)}
              value={value}
            />
          )}
          name="password"
          rules={{required: true, minLength: 6}}
          defaultValue=""
        />

        {errors.confirmPassword && (
          <Text style={styles.errorText}>
            {t('common.errors.confirmPasswordError')}
          </Text>
        )}
        <Controller
          control={control}
          render={({onChange, onBlur, value}) => (
            <TextInput
              style={styles.inputStyle}
              placeholder={t('confirmation')}
              maxLength={15}
              secureTextEntry={true}
              onBlur={onBlur}
              onChangeText={(value) => onChange(value)}
              value={value}
            />
          )}
          name="confirmPassword"
          rules={{required: true, validate: validatePwdConfirmation}}
          defaultValue=""
        />

        <Button
          color={Colors.primary}
          title={t('register')}
          disabled={!formState.isValid}
          onPress={handleSubmit(handleOnPress)}
        />

        <Text
          style={styles.loginText}
          onPress={() =>
            navigation.navigate('login', {...(backTo && {backTo: backTo})})
          }>
          {t('register_message')}
        </Text>
        {alertMessage && showAlert && (
          <CustomAlert
            message={alertMessage}
            showAlert={showAlert}
            confirmText="OK"
            onCancelPressed={() => {
              setShowAlert(false);
            }}
            onConfirmPressed={()=>{handleOnConfirmPressed()}}
          />
        )}
      </View>
    </KeyboardAvoidingView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    padding: 35,
    backgroundColor: '#fff',
  },
  inputStyle: {
    width: '100%',
    marginBottom: 15,
    paddingBottom: 15,
    alignSelf: 'center',
    borderColor: Colors.primary,
    borderBottomWidth: 1,
  },
  loginText: {
    color: Colors.primary,
    marginTop: 25,
    textAlign: 'center',
  },
  errorText: {
    color: Colors.red,
  },
  preloader: {
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff',
  },
});

export default RegisterScreen;
