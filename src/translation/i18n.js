import i18next from 'i18next';
import {initReactI18next} from 'react-i18next';
import fr from './fr';
import en from './en';
import {getLanguage} from './lang';

const resources = {
  en: {
    translation: en,
  },
  fr: {
    translation: fr,
  },
};

i18next.use(initReactI18next).init({
  resources: resources,
  lng: getLanguage(),
  fallbackLng: 'fr',
  debug: true,
  interpolation: {
    escapeValue: false,
  },
});
