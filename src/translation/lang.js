import * as RNLocalize from 'react-native-localize';

const fallbackLanguage = 'fr';
const supportedLanguages = ['en', 'fr'];

export const getLanguage = () => {
  const language = RNLocalize.findBestAvailableLanguage(supportedLanguages);
  return language ? language.languageTag : fallbackLanguage;
};
